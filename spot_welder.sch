EESchema Schematic File Version 4
LIBS:spot_welder-cache
EELAYER 29 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Cheapskate spot welder"
Date "2019-04-16"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 "With help from: swiftgeek,DocScrutinizer05"
Comment4 "Author: Fedja Beader"
$EndDescr
Text Notes 2450 750  0    157  ~ 0
Power side
$Comp
L power:GND #PWR031
U 1 1 5CB00265
P 3850 8100
F 0 "#PWR031" H 3850 7850 50  0001 C CNN
F 1 "GND" H 3850 7950 50  0000 C CNN
F 2 "" H 3850 8100 50  0001 C CNN
F 3 "" H 3850 8100 50  0001 C CNN
	1    3850 8100
	1    0    0    -1  
$EndComp
Text Label 3700 7700 0    60   ~ 0
gate_drv
$Comp
L Diode:1.5KExxA D6
U 1 1 5CB039EE
P 5300 8000
F 0 "D6" H 5300 8100 50  0000 C CNN
F 1 "TVS SMDJ13A" H 5300 7900 50  0000 C CNN
F 2 "Diode_SMD:D_SMC" H 5300 8000 50  0001 C CNN
F 3 "" H 5300 8000 50  0000 C CNN
	1    5300 8000
	0    -1   1    0   
$EndComp
$Comp
L power:GND #PWR037
U 1 1 5CB042A1
P 5300 8250
F 0 "#PWR037" H 5300 8000 50  0001 C CNN
F 1 "GND" H 5300 8100 50  0000 C CNN
F 2 "" H 5300 8250 50  0001 C CNN
F 3 "" H 5300 8250 50  0001 C CNN
	1    5300 8250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 7700 5000 7800
Text Notes 14550 750  0    157  ~ 0
misc.
$Comp
L power:GND #PWR063
U 1 1 5CE24C28
P 10850 3650
F 0 "#PWR063" H 10850 3400 50  0001 C CNN
F 1 "GND" H 10855 3477 50  0000 C CNN
F 2 "" H 10850 3650 50  0001 C CNN
F 3 "" H 10850 3650 50  0001 C CNN
	1    10850 3650
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR062
U 1 1 5CE25897
P 10850 3050
F 0 "#PWR062" H 10850 2900 50  0001 C CNN
F 1 "VCC" H 10867 3223 50  0000 C CNN
F 2 "" H 10850 3050 50  0001 C CNN
F 3 "" H 10850 3050 50  0001 C CNN
	1    10850 3050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5CE25F37
P 10700 3350
F 0 "C8" H 10725 3450 50  0000 L CNN
F 1 "100nF" H 10725 3250 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 10738 3200 50  0001 C CNN
F 3 "" H 10700 3350 50  0001 C CNN
F 4 "K104K15X7RF53L2" H 1800 1600 50  0001 C CNN "part"
	1    10700 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	11100 3050 10850 3050
Text Label 5100 7700 0    59   ~ 0
PWR_gate_drv
Text Notes 4050 8400 0    39   ~ 0
TODO: is this TVS really necessary?\n(was in some other schematics)\nnote: MOSFETs have a Vgs of +- 20V.
$Comp
L power:VCC #PWR064
U 1 1 5D017DF6
P 12150 1200
F 0 "#PWR064" H 12150 1050 50  0001 C CNN
F 1 "VCC" H 12167 1373 50  0000 C CNN
F 2 "" H 12150 1200 50  0001 C CNN
F 3 "" H 12150 1200 50  0001 C CNN
	1    12150 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR065
U 1 1 5D018B70
P 12200 1600
F 0 "#PWR065" H 12200 1350 50  0001 C CNN
F 1 "GND" H 12205 1427 50  0000 C CNN
F 2 "" H 12200 1600 50  0001 C CNN
F 3 "" H 12200 1600 50  0001 C CNN
	1    12200 1600
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR075
U 1 1 5D01C017
P 13900 1200
F 0 "#PWR075" H 13900 1050 50  0001 C CNN
F 1 "+3V3" H 13915 1373 50  0000 C CNN
F 2 "" H 13900 1200 50  0001 C CNN
F 3 "" H 13900 1200 50  0001 C CNN
	1    13900 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	12150 1200 12200 1200
$Comp
L Device:C C10
U 1 1 5D026193
P 12200 1350
F 0 "C10" H 12315 1396 50  0000 L CNN
F 1 "100nF" H 12315 1305 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 12238 1200 50  0001 C CNN
F 3 "~" H 12200 1350 50  0001 C CNN
F 4 "K104K15X7RF53L2" H 2050 -250 50  0001 C CNN "part"
	1    12200 1350
	1    0    0    -1  
$EndComp
Connection ~ 12200 1200
Wire Wire Line
	12200 1200 12600 1200
Wire Wire Line
	12200 1600 12200 1500
Connection ~ 13300 1200
Wire Wire Line
	13300 1200 13250 1200
$Comp
L Device:C C13
U 1 1 5D04EB2B
P 13300 1350
F 0 "C13" H 13415 1396 50  0000 L CNN
F 1 "100nF" H 13415 1305 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 13338 1200 50  0001 C CNN
F 3 "~" H 13300 1350 50  0001 C CNN
F 4 "K104K15X7RF53L2" H 1850 -250 50  0001 C CNN "part"
	1    13300 1350
	1    0    0    -1  
$EndComp
Text Notes 12050 950  0    79   ~ 0
MCU power
Wire Notes Line
	12000 1850 12000 800 
$Comp
L Device:R R45
U 1 1 5CC3D447
P 12750 3250
F 0 "R45" V 12830 3250 50  0000 C CNN
F 1 "1k" V 12750 3250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 12680 3250 50  0001 C CNN
F 3 "" H 12750 3250 50  0001 C CNN
F 4 "MCF 0.25W 1K" H -1900 2100 50  0001 C CNN "part"
F 5 "have, C57435" H 3000 -2150 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1001A50_C57435.html" H 3000 -2150 50  0001 C CNN "LCSC_web"
	1    12750 3250
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 5CC3D450
P 12750 3600
F 0 "SW1" H 12750 3835 50  0000 C CNN
F 1 "SW_SPST" H 12750 3744 50  0000 C CNN
F 2 "" H 12750 3600 50  0001 C CNN
F 3 "~" H 12750 3600 50  0001 C CNN
F 4 "PTS125SM43LFS" H 12750 3600 50  0001 C CNN "part"
	1    12750 3600
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR071
U 1 1 5CC3D43F
P 13750 3100
F 0 "#PWR071" H 13750 2950 50  0001 C CNN
F 1 "+3V3" H 13765 3273 50  0000 C CNN
F 2 "" H 13750 3100 50  0001 C CNN
F 3 "" H 13750 3100 50  0001 C CNN
	1    13750 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R46
U 1 1 5CD2A4BA
P 13750 3250
F 0 "R46" V 13830 3250 50  0000 C CNN
F 1 "1k" V 13750 3250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 13680 3250 50  0001 C CNN
F 3 "" H 13750 3250 50  0001 C CNN
F 4 "MCF 0.25W 1K" H -1950 2100 50  0001 C CNN "part"
F 5 "have, C57435" H 2950 -2150 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1001A50_C57435.html" H 2950 -2150 50  0001 C CNN "LCSC_web"
	1    13750 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR068
U 1 1 5CD96CD0
P 12750 3800
F 0 "#PWR068" H 12750 3550 50  0001 C CNN
F 1 "GND" H 12755 3627 50  0000 C CNN
F 2 "" H 12750 3800 50  0001 C CNN
F 3 "" H 12750 3800 50  0001 C CNN
	1    12750 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J9
U 1 1 5CDE8968
P 15050 4100
F 0 "J9" H 15158 4381 50  0000 C CNN
F 1 "UART Conn" H 15158 4290 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 15050 4100 50  0001 C CNN
F 3 "~" H 15050 4100 50  0001 C CNN
F 4 "Already have" H 1100 1500 50  0001 C CNN "part"
	1    15050 4100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR081
U 1 1 5CE01076
P 15850 4100
F 0 "#PWR081" H 15850 3850 50  0001 C CNN
F 1 "GND" H 15855 3927 50  0000 C CNN
F 2 "" H 15850 4100 50  0001 C CNN
F 3 "" H 15850 4100 50  0001 C CNN
	1    15850 4100
	1    0    0    -1  
$EndComp
Text Label 15400 4000 0    39   ~ 0
USART_TX
Text Label 15400 4200 0    39   ~ 0
USART_RX
Wire Wire Line
	15250 4000 15650 4000
Wire Wire Line
	15650 4200 15250 4200
Wire Wire Line
	15250 4100 15850 4100
Text Label 12350 3250 1    39   ~ 0
SWD_NRST
$Comp
L Device:C C11
U 1 1 5CF8E12D
P 12350 3550
F 0 "C11" H 12465 3596 50  0000 L CNN
F 1 "100nF" H 12465 3505 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 12388 3400 50  0001 C CNN
F 3 "~" H 12350 3550 50  0001 C CNN
F 4 "K104K15X7RF53L2" H -1900 2100 50  0001 C CNN "part"
	1    12350 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	12350 3400 12750 3400
Wire Wire Line
	12350 3700 12350 3800
Wire Wire Line
	12350 3800 12750 3800
Connection ~ 12750 3800
Connection ~ 12750 3400
$Comp
L power:+3V3 #PWR067
U 1 1 5CFD6B98
P 12750 3100
F 0 "#PWR067" H 12750 2950 50  0001 C CNN
F 1 "+3V3" H 12765 3273 50  0000 C CNN
F 2 "" H 12750 3100 50  0001 C CNN
F 3 "" H 12750 3100 50  0001 C CNN
	1    12750 3100
	1    0    0    -1  
$EndComp
Connection ~ 12350 3400
Connection ~ 10850 3650
Wire Wire Line
	10850 3650 11100 3650
Connection ~ 10850 3050
Wire Wire Line
	4100 7900 4100 8100
Wire Wire Line
	4100 8100 5000 8100
Wire Wire Line
	5000 8100 5000 7900
NoConn ~ 4100 7800
Wire Wire Line
	13300 1600 13300 1500
Wire Wire Line
	3700 7700 4100 7700
Text Label 12000 3400 0    39   ~ 0
mcu_reset
Wire Wire Line
	12350 2950 12350 3400
Text Notes 12950 3250 0    39   ~ 0
BOOT0=1 enables\nbootloader on a vanilla\ndevice (nBOOT1=1)\nUSART, I2C, USB DFU\n(USART only on f070)
Text Notes 8500 650  0    157  ~ 0
Measurement
Text Notes 6800 5950 0    39   ~ 0
https://www.analog.com/en/technical-articles/op-amp-precision-positive-negative-clipper-using-lt6015-lt6016-lt6017.html
$Comp
L Device:CP C15
U 1 1 5CB64EDC
P 13800 1350
F 0 "C15" H 13918 1396 50  0000 L CNN
F 1 "100uF" H 13918 1305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 13838 1200 50  0001 C CNN
F 3 "~" H 13800 1350 50  0001 C CNN
	1    13800 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	13800 1500 13800 1600
Wire Wire Line
	13800 1600 13300 1600
Connection ~ 13300 1600
Text Notes 13850 1700 0    39   ~ 0
4.7uF would\nhave been\nenough.
Wire Notes Line
	15200 800  15200 1850
$Comp
L Jumper:Jumper_3_Bridged12 JP5
U 1 1 5CC0ED5A
P 13750 3650
F 0 "JP5" H 13600 3550 50  0000 C CNN
F 1 "Jumper_3_Bridged12" H 13750 3800 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x03_P2.54mm_Vertical" H 13750 3650 50  0001 C CNN
F 3 "~" H 13750 3650 50  0001 C CNN
F 4 "n/a" H -1950 2100 50  0001 C CNN "part"
	1    13750 3650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR072
U 1 1 5CC866BD
P 13750 3900
F 0 "#PWR072" H 13750 3650 50  0001 C CNN
F 1 "GND" H 13755 3727 50  0000 C CNN
F 2 "" H 13750 3900 50  0001 C CNN
F 3 "" H 13750 3900 50  0001 C CNN
	1    13750 3900
	1    0    0    -1  
$EndComp
Text Label 13300 3650 0    39   ~ 0
mcu_boot0
Wire Wire Line
	13300 3650 13600 3650
$Comp
L power:GND #PWR045
U 1 1 5CD6C222
P 7150 6900
F 0 "#PWR045" H 7150 6650 50  0001 C CNN
F 1 "GND" H 7150 6750 50  0000 C CNN
F 2 "" H 7150 6900 50  0001 C CNN
F 3 "" H 7150 6900 50  0001 C CNN
	1    7150 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9250 6600 9300 6600
Connection ~ 9250 6600
Wire Wire Line
	9250 6850 9250 6600
Text Notes 6800 7500 0    39   ~ 0
Zener is probably not necessary (they leak!),\nADC diodes can clip it\n(they exist, see p87 of f072rb datasheet, positive\n injection does not harm accuracy, but has to be\n burned off somehow; It is expected that the mcu\n will be easily able to burn the transient injection)
Wire Notes Line
	8550 6100 6750 6100
Text Notes 7600 7050 0    39   ~ 0
U=14.4 U_batt = 3.35\nU=13   U_batt = 3.02\n3.5mV steps\nC = 10e-9\nt = C * Rb*Rt / (Rb+Rt)\n1.5 ms to charge cap
Wire Notes Line
	11450 6100 11450 7400
Wire Wire Line
	9650 6850 10100 6850
Wire Wire Line
	10950 6850 11000 6850
Text Notes 10050 6600 0    39   ~ 0
U = 3V, R = 1000\nC = 10e-9\nt = C * U / (U/R)\n10 us to charge cap
$Comp
L power:GND #PWR061
U 1 1 5CD359B5
P 10100 7150
F 0 "#PWR061" H 10100 6900 50  0001 C CNN
F 1 "GND" H 10100 7000 50  0000 C CNN
F 2 "" H 10100 7150 50  0001 C CNN
F 3 "" H 10100 7150 50  0001 C CNN
	1    10100 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5CD359AF
P 10100 7000
F 0 "C7" H 10125 7100 50  0000 L CNN
F 1 "10nF" H 10100 6900 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 10138 6850 50  0001 C CNN
F 3 "" H 10100 7000 50  0001 C CNN
F 4 "MCFYU5103Z5" H 1800 2850 50  0001 C CNN "part"
	1    10100 7000
	1    0    0    -1  
$EndComp
Text Notes 6750 6250 0    79   ~ 0
Battery voltage measurement
Text Notes 11150 6950 0    39   ~ 0
to FT I/O
Wire Notes Line
	8650 6100 8650 7400
Text Notes 9050 7250 0    39   ~ 0
zener breakdown\nvoltage > Vih
Wire Wire Line
	9150 6600 9250 6600
Text Notes 9100 6400 0    39   ~ 0
line charge
Wire Wire Line
	8750 6600 8850 6600
$Comp
L Device:R R41
U 1 1 5CBA77FB
P 9000 6600
F 0 "R41" V 9080 6600 50  0000 C CNN
F 1 "1k" V 9000 6600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8930 6600 50  0001 C CNN
F 3 "" H 9000 6600 50  0001 C CNN
F 4 "MCF 0.25W 1K" H 1850 2850 50  0001 C CNN "part"
F 5 "have, C57435" H 1850 2850 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1001A50_C57435.html" H 1850 2850 50  0001 C CNN "LCSC_web"
	1    9000 6600
	0    1    1    0   
$EndComp
Wire Wire Line
	9300 6500 9150 6500
Wire Wire Line
	8850 6500 8750 6500
$Comp
L power:+BATT #PWR056
U 1 1 5CB7F0E8
P 8750 6500
F 0 "#PWR056" H 8750 6350 50  0001 C CNN
F 1 "+BATT" H 8765 6673 50  0000 C CNN
F 2 "" H 8750 6500 50  0001 C CNN
F 3 "" H 8750 6500 50  0001 C CNN
	1    8750 6500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5CB71EF5
P 9500 6500
F 0 "J6" H 9580 6492 50  0000 L CNN
F 1 "Foot switch" H 9580 6401 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Horizontal" H 9500 6500 50  0001 C CNN
F 3 "~" H 9500 6500 50  0001 C CNN
F 4 "Already have (ali)" H 1850 2850 50  0001 C CNN "part"
	1    9500 6500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR057
U 1 1 5CB6D4D8
P 8750 6600
F 0 "#PWR057" H 8750 6350 50  0001 C CNN
F 1 "GND" H 8750 6450 50  0000 C CNN
F 2 "" H 8750 6600 50  0001 C CNN
F 3 "" H 8750 6600 50  0001 C CNN
	1    8750 6600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R40
U 1 1 5CB6D4D1
P 9000 6500
F 0 "R40" V 9080 6500 50  0000 C CNN
F 1 "10k" V 9000 6500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8930 6500 50  0001 C CNN
F 3 "" H 9000 6500 50  0001 C CNN
F 4 "MCF 0.25W 10K" H 1850 2850 50  0001 C CNN "part"
F 5 "have, C57436" H 1850 2850 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1002A50_C57436.html" H 1850 2850 50  0001 C CNN "LCSC_web"
	1    9000 6500
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR060
U 1 1 5CC3C7C7
P 9650 7150
F 0 "#PWR060" H 9650 6900 50  0001 C CNN
F 1 "GND" H 9655 6977 50  0000 C CNN
F 2 "" H 9650 7150 50  0001 C CNN
F 3 "" H 9650 7150 50  0001 C CNN
	1    9650 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D16
U 1 1 5CC26838
P 9650 7000
F 0 "D16" V 9604 7079 50  0000 L CNN
F 1 "2-3V" V 9695 7079 50  0000 L CNN
F 2 "Diode_SMD:D_MiniMELF_Handsoldering" H 9650 7000 50  0001 C CNN
F 3 "~" H 9650 7000 50  0001 C CNN
F 4 "MMBZ5223B" V 9650 7000 50  0001 C CNN "part"
	1    9650 7000
	0    1    1    0   
$EndComp
Text Notes 8650 6250 0    79   ~ 0
Detect foot switch pressed
Text Label 10950 6850 0    60   ~ 0
foot_sense
Text Label 7700 6600 0    59   ~ 0
U_batt
$Comp
L power:GND #PWR044
U 1 1 5CAD2BCB
P 6900 6900
F 0 "#PWR044" H 6900 6650 50  0001 C CNN
F 1 "GND" H 6900 6750 50  0000 C CNN
F 2 "" H 6900 6900 50  0001 C CNN
F 3 "" H 6900 6900 50  0001 C CNN
	1    6900 6900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5CAD2A85
P 6900 6750
F 0 "C6" H 6925 6850 50  0000 L CNN
F 1 "10nF" H 6925 6650 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 6938 6600 50  0001 C CNN
F 3 "" H 6900 6750 50  0001 C CNN
F 4 "MCFYU5103Z5" H 1550 2650 50  0001 C CNN "part"
	1    6900 6750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR052
U 1 1 5CAD29D3
P 8400 6450
F 0 "#PWR052" H 8400 6200 50  0001 C CNN
F 1 "GND" H 8400 6300 50  0000 C CNN
F 2 "" H 8400 6450 50  0001 C CNN
F 3 "" H 8400 6450 50  0001 C CNN
	1    8400 6450
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR043
U 1 1 5CAD2725
P 6900 6450
F 0 "#PWR043" H 6900 6300 50  0001 C CNN
F 1 "+BATT" H 6900 6590 50  0000 C CNN
F 2 "" H 6900 6450 50  0001 C CNN
F 3 "" H 6900 6450 50  0001 C CNN
	1    6900 6450
	1    0    0    -1  
$EndComp
Text Notes 15500 3650 0    39   ~ 0
SWO
NoConn ~ 15450 3650
Wire Wire Line
	15850 3350 15450 3350
$Comp
L power:GND #PWR083
U 1 1 5CEFB353
P 15850 3350
F 0 "#PWR083" H 15850 3100 50  0001 C CNN
F 1 "GND" H 15855 3177 50  0000 C CNN
F 2 "" H 15850 3350 50  0001 C CNN
F 3 "" H 15850 3350 50  0001 C CNN
	1    15850 3350
	1    0    0    -1  
$EndComp
Text Label 15450 3550 0    39   ~ 0
SWD_NRST
Text Label 15450 3450 0    39   ~ 0
SWDIO
Text Label 15450 3250 0    39   ~ 0
SWCLK
Wire Wire Line
	15600 3150 15450 3150
$Comp
L power:+3V3 #PWR082
U 1 1 5CEE52D8
P 15600 3150
F 0 "#PWR082" H 15600 3000 50  0001 C CNN
F 1 "+3V3" H 15615 3323 50  0000 C CNN
F 2 "" H 15600 3150 50  0001 C CNN
F 3 "" H 15600 3150 50  0001 C CNN
	1    15600 3150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x06_Male J10
U 1 1 5CED22FC
P 15250 3350
F 0 "J10" H 15358 3731 50  0000 C CNN
F 1 "SWD_conn" H 15358 3640 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Horizontal" H 15250 3350 50  0001 C CNN
F 3 "~" H 15250 3350 50  0001 C CNN
F 4 "Already have" H 50  600 50  0001 C CNN "part"
	1    15250 3350
	1    0    0    -1  
$EndComp
Text Notes 11950 2550 0    60   ~ 0
Other stuff to get that are not part of this BOM:\n- Foot switch ($3)\n- wires ($?)\n- battery terminals ($2)\n- battery ($0 - see your mobile polluting machine)\n- USB-uart ($0.5)
Text Notes 14600 2350 0    39   ~ 0
STM32f072 data:\nVih@FT = 1.85 V min\nVih@TTa = 1.86 V min\nVil@TTa = 1.06V max\nVil@FT = 1.37V max\nsee page 78 of dev ds
Text Notes 12150 2850 0    79   ~ 16
MCU reset & boot selection
Wire Notes Line
	11950 4150 11950 2700
Text Notes 14200 2850 0    79   ~ 16
MCU communication
Wire Wire Line
	13300 1200 13800 1200
Wire Wire Line
	13900 1200 13800 1200
Connection ~ 13800 1200
$Comp
L power:+3V3 #PWR069
U 1 1 5CEE0164
P 13500 7700
F 0 "#PWR069" H 13500 7550 50  0001 C CNN
F 1 "+3V3" H 13515 7873 50  0000 C CNN
F 2 "" H 13500 7700 50  0001 C CNN
F 3 "" H 13500 7700 50  0001 C CNN
	1    13500 7700
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3VADC #PWR079
U 1 1 5CEE0174
P 14850 7900
F 0 "#PWR079" H 15000 7850 50  0001 C CNN
F 1 "+3.3VADC" H 14870 8043 50  0000 C CNN
F 2 "" H 14850 7900 50  0001 C CNN
F 3 "" H 14850 7900 50  0001 C CNN
	1    14850 7900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C18
U 1 1 5CEE017A
P 14350 7850
F 0 "C18" H 14465 7896 50  0000 L CNN
F 1 "100nF" H 14465 7805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 14388 7700 50  0001 C CNN
F 3 "~" H 14350 7850 50  0001 C CNN
F 4 "K104K15X7RF53L2" H 50  600 50  0001 C CNN "part"
	1    14350 7850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5CEE0180
P 13900 7850
F 0 "C16" H 14015 7896 50  0000 L CNN
F 1 "100nF" H 14015 7805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 13938 7700 50  0001 C CNN
F 3 "~" H 13900 7850 50  0001 C CNN
F 4 "K104K15X7RF53L2" H 50  600 50  0001 C CNN "part"
	1    13900 7850
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 5CEE0186
P 13450 7850
F 0 "C14" H 13565 7896 50  0000 L CNN
F 1 "100nF" H 13565 7805 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 13488 7700 50  0001 C CNN
F 3 "~" H 13450 7850 50  0001 C CNN
F 4 "K104K15X7RF53L2" H 50  600 50  0001 C CNN "part"
	1    13450 7850
	1    0    0    -1  
$EndComp
Wire Wire Line
	13450 7700 13500 7700
Wire Wire Line
	13900 7700 14350 7700
Connection ~ 13900 7700
Wire Wire Line
	14350 8000 13900 8000
Wire Wire Line
	13900 8000 13500 8000
Connection ~ 13900 8000
$Comp
L power:GND #PWR070
U 1 1 5CEE0196
P 13500 8000
F 0 "#PWR070" H 13500 7750 50  0001 C CNN
F 1 "GND" H 13505 7827 50  0000 C CNN
F 2 "" H 13500 8000 50  0001 C CNN
F 3 "" H 13500 8000 50  0001 C CNN
	1    13500 8000
	1    0    0    -1  
$EndComp
Connection ~ 13500 8000
Wire Wire Line
	13500 8000 13450 8000
Connection ~ 13500 7700
Wire Wire Line
	13500 7700 13900 7700
Wire Wire Line
	15200 7700 14350 7700
Connection ~ 14350 7700
Text Label 6800 2200 0    59   ~ 0
MOSFET_drain
Text Label 6800 2500 0    59   ~ 0
MOSFET_source
Wire Wire Line
	6800 2200 7500 2200
Wire Wire Line
	6800 2500 7500 2500
Text Label 10400 2350 0    59   ~ 0
U_rdson
$Comp
L power:VCC #PWR053
U 1 1 5DA2FAAB
P 8450 1250
F 0 "#PWR053" H 8450 1100 50  0001 C CNN
F 1 "VCC" H 8467 1423 50  0000 C CNN
F 2 "" H 8450 1250 50  0001 C CNN
F 3 "" H 8450 1250 50  0001 C CNN
	1    8450 1250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR054
U 1 1 5DA312A8
P 8450 1900
F 0 "#PWR054" H 8450 1650 50  0001 C CNN
F 1 "GND" H 8455 1727 50  0000 C CNN
F 2 "" H 8450 1900 50  0001 C CNN
F 3 "" H 8450 1900 50  0001 C CNN
	1    8450 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR048
U 1 1 5DA32981
P 7900 1950
F 0 "#PWR048" H 7900 1700 50  0001 C CNN
F 1 "GND" H 7905 1777 50  0000 C CNN
F 2 "" H 7900 1950 50  0001 C CNN
F 3 "" H 7900 1950 50  0001 C CNN
	1    7900 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	8450 1250 8450 1300
Wire Wire Line
	7900 1950 7900 1900
Wire Wire Line
	8150 1600 8150 2200
Connection ~ 8150 2200
Wire Wire Line
	8550 2200 8550 2250
Wire Wire Line
	8150 2200 8550 2200
Wire Wire Line
	8550 2500 8550 2450
Wire Wire Line
	8250 1600 8250 2500
Connection ~ 8250 2500
Wire Wire Line
	8250 2500 8400 2500
Text Notes 8850 1650 0    39   ~ 0
Problems with schottkys to Vcc/GND:\n- There is a voltage difference before they act, which could be\n  much greater than the opamp internal diodes (0.3V?).\n- Conductance to Vcc lifts the Vcc supply, which in this case is able to sink\n  the current, but introduces noise\n\nProblems with TVS between + and -:\n- leakage current affects measurements? Should be insignificant \n  at 0.2V drop for a 13V standoff TVS.\n- TVS might not activate fast enough to protect the opamp, or\n  if a lower voltage one is selected, see above.
$Comp
L Diode:1N4148 D17
U 1 1 5DC13B07
P 10150 2200
F 0 "D17" H 10150 2416 50  0000 C CNN
F 1 "1N4148" H 10150 2325 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10150 2025 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10150 2200 50  0001 C CNN
F 4 "Diotec 1N4148" H 1850 -100 50  0001 C CNN "part"
	1    10150 2200
	0    1    1    0   
$EndComp
Wire Wire Line
	9150 2350 9200 2350
Wire Wire Line
	9500 2350 9500 2150
Connection ~ 9200 2350
$Comp
L power:+3V3 #PWR058
U 1 1 5DCBC2A3
P 9350 1950
F 0 "#PWR058" H 9350 1800 50  0001 C CNN
F 1 "+3V3" H 9365 2123 50  0000 C CNN
F 2 "" H 9350 1950 50  0001 C CNN
F 3 "" H 9350 1950 50  0001 C CNN
	1    9350 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 1950 9500 1950
Wire Wire Line
	10100 2050 10150 2050
Wire Wire Line
	10150 2350 10750 2350
Text Notes 6850 1050 0    39   ~ 0
Electrical over stress protection\n- not necessary? max negative injection is 50mA
$Comp
L power:+3V3 #PWR073
U 1 1 5DEB1A9D
P 13850 6950
F 0 "#PWR073" H 13850 6800 50  0001 C CNN
F 1 "+3V3" H 13865 7123 50  0000 C CNN
F 2 "" H 13850 6950 50  0001 C CNN
F 3 "" H 13850 6950 50  0001 C CNN
	1    13850 6950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR074
U 1 1 5DEB1AA3
P 13850 7250
F 0 "#PWR074" H 13850 7000 50  0001 C CNN
F 1 "GND" H 13855 7077 50  0000 C CNN
F 2 "" H 13850 7250 50  0001 C CNN
F 3 "" H 13850 7250 50  0001 C CNN
	1    13850 7250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 5DEB1AA9
P 14000 7100
F 0 "C17" H 14115 7146 50  0000 L CNN
F 1 "10nF" H 14115 7055 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 14038 6950 50  0001 C CNN
F 3 "~" H 14000 7100 50  0001 C CNN
F 4 "MCFYU5103Z5" H 1250 5500 50  0001 C CNN "part"
	1    14000 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	14150 6950 14000 6950
Connection ~ 14000 6950
Wire Wire Line
	14000 6950 13850 6950
$Comp
L power:GNDA #PWR077
U 1 1 5DEB1AB2
P 14150 7250
F 0 "#PWR077" H 14150 7000 50  0001 C CNN
F 1 "GNDA" H 14155 7077 50  0000 C CNN
F 2 "" H 14150 7250 50  0001 C CNN
F 3 "" H 14150 7250 50  0001 C CNN
	1    14150 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	14150 7250 14000 7250
Connection ~ 14000 7250
Wire Wire Line
	14000 7250 13850 7250
$Comp
L power:+3.3VADC #PWR076
U 1 1 5DEB1ABB
P 14150 6950
F 0 "#PWR076" H 14300 6900 50  0001 C CNN
F 1 "+3.3VADC" H 14170 7093 50  0000 C CNN
F 2 "" H 14150 6950 50  0001 C CNN
F 3 "" H 14150 6950 50  0001 C CNN
	1    14150 6950
	1    0    0    -1  
$EndComp
Text Notes 3750 8750 0    39   ~ 0
gate driver: energy storage (capacitors) must supply enough energy\nto turn the fets on and off. Max 6A*1us = 6 uAs\n\nLet's say we allow 2V drop (12->10V), then 12 uF will be required.
Text Notes 9250 2750 0    39   ~ 0
20 dB gain required for 0.2 mohm combined RDSon\nVs = Ve (1 + R1/R2) = (1500A * 0.0002) * (1 + 100k/10k) = 3.3V\n\nAssuming 20pF combined ADC capacitance,\nwe need 2uA to register 100mV change in 1 microsecond
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 5E1FC03F
P 4950 5200
F 0 "J4" H 5058 5381 50  0000 C CNN
F 1 "conn_smallwire" H 5058 5290 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4950 5200 50  0001 C CNN
F 3 "~" H 4950 5200 50  0001 C CNN
	1    4950 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 5200 5150 5200
$Comp
L Connector:Conn_01x01_Male J1
U 1 1 5E29F7B0
P 3900 4900
F 0 "J1" H 4008 5081 50  0000 C CNN
F 1 "conn 10mm^2" H 4008 4990 50  0000 C CNN
F 2 "" H 3900 4900 50  0001 C CNN
F 3 "~" H 3900 4900 50  0001 C CNN
F 4 "n/a" H 1550 3500 50  0001 C CNN "part"
	1    3900 4900
	1    0    0    -1  
$EndComp
Text Label 4350 5200 0    39   ~ 0
weld-
$Comp
L Connector:Conn_01x01_Male J2
U 1 1 5E2B3CEF
P 3900 5200
F 0 "J2" H 4008 5381 50  0000 C CNN
F 1 "Conn_10mm^2" H 4008 5290 50  0000 C CNN
F 2 "" H 3900 5200 50  0001 C CNN
F 3 "~" H 3900 5200 50  0001 C CNN
F 4 "n/a" H 1550 3500 50  0001 C CNN "part"
	1    3900 5200
	1    0    0    -1  
$EndComp
Text Notes 3750 4650 0    79   ~ 0
Power connectors
$Comp
L power:GNDPWR #PWR036
U 1 1 5E7EA193
P 4450 4900
F 0 "#PWR036" H 4450 4700 50  0001 C CNN
F 1 "GNDPWR" H 4454 4746 50  0000 C CNN
F 2 "" H 4450 4850 50  0001 C CNN
F 3 "" H 4450 4850 50  0001 C CNN
	1    4450 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4900 4100 4900
Wire Wire Line
	4500 5200 4100 5200
$Comp
L power:+BATT #PWR040
U 1 1 5E8ECB3A
P 5700 5200
F 0 "#PWR040" H 5700 5050 50  0001 C CNN
F 1 "+BATT" H 5715 5373 50  0000 C CNN
F 2 "" H 5700 5200 50  0001 C CNN
F 3 "" H 5700 5200 50  0001 C CNN
	1    5700 5200
	1    0    0    -1  
$EndComp
Wire Notes Line
	3650 4500 5900 4500
Text Notes 6800 850  0    79   ~ 0
Current sense amplifier to increase ADC accuracy
Wire Notes Line
	3650 8800 5900 8800
Wire Notes Line
	3650 5700 5900 5700
Wire Notes Line
	5900 4500 5900 5700
Wire Notes Line
	3650 6600 3650 5800
Wire Notes Line
	3650 5800 5550 5800
Text Notes 4050 6600 0    39   ~ 0
See TI EOS-2 lecture (or not)
Text Notes 4150 5950 0    79   ~ 0
Board PSU
Connection ~ 4650 6050
$Comp
L Device:CP C5
U 1 1 5DEB1A86
P 4650 6200
F 0 "C5" H 4768 6246 50  0000 L CNN
F 1 "1uF" H 4768 6155 50  0000 L CNN
F 2 "" H 4688 6050 50  0001 C CNN
F 3 "~" H 4650 6200 50  0001 C CNN
	1    4650 6200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR038
U 1 1 5DEB1A80
P 5400 6050
F 0 "#PWR038" H 5400 5900 50  0001 C CNN
F 1 "VCC" H 5417 6223 50  0000 C CNN
F 2 "" H 5400 6050 50  0001 C CNN
F 3 "" H 5400 6050 50  0001 C CNN
	1    5400 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR039
U 1 1 5DEB1A7A
P 5400 6350
F 0 "#PWR039" H 5400 6100 50  0001 C CNN
F 1 "GND" H 5405 6177 50  0000 C CNN
F 2 "" H 5400 6350 50  0001 C CNN
F 3 "" H 5400 6350 50  0001 C CNN
	1    5400 6350
	1    0    0    -1  
$EndComp
$Comp
L power:-BATT #PWR030
U 1 1 5DEB1A74
P 3800 6350
F 0 "#PWR030" H 3800 6200 50  0001 C CNN
F 1 "-BATT" H 3815 6523 50  0000 C CNN
F 2 "" H 3800 6350 50  0001 C CNN
F 3 "" H 3800 6350 50  0001 C CNN
	1    3800 6350
	1    0    0    1   
$EndComp
$Comp
L power:+BATT #PWR029
U 1 1 5DEB1A6E
P 3800 6050
F 0 "#PWR029" H 3800 5900 50  0001 C CNN
F 1 "+BATT" H 3815 6223 50  0000 C CNN
F 2 "" H 3800 6050 50  0001 C CNN
F 3 "" H 3800 6050 50  0001 C CNN
	1    3800 6050
	1    0    0    -1  
$EndComp
Wire Notes Line
	3650 6700 5900 6700
$Comp
L power:VCC #PWR032
U 1 1 5CE78A6E
P 3900 6950
F 0 "#PWR032" H 3900 6800 50  0001 C CNN
F 1 "VCC" H 3917 7123 50  0000 C CNN
F 2 "" H 3900 6950 50  0001 C CNN
F 3 "" H 3900 6950 50  0001 C CNN
	1    3900 6950
	1    0    0    -1  
$EndComp
Connection ~ 5000 7700
$Comp
L mcp1407:MCP1407 U1
U 1 1 5CB5B30C
P 4550 7750
F 0 "U1" H 4250 8000 60  0000 C CNN
F 1 "MCP1407" H 4700 8000 60  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 4450 7500 60  0000 C CNN
F 3 "" H 4550 7500 60  0001 C CNN
F 4 "MCP1407-E/P" H 1550 3400 50  0001 C CNN "part"
F 5 "MCP1407-E/P" H 1550 3150 50  0001 C CNN "arrow"
	1    4550 7750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 7350 5000 7600
Wire Wire Line
	4100 7350 5000 7350
Wire Wire Line
	4100 7600 4100 7350
Text Notes 4400 6850 0    79   ~ 0
GATE DRIVER
$Comp
L power:GND #PWR033
U 1 1 5CAFD58F
P 3900 7300
F 0 "#PWR033" H 3900 7050 50  0001 C CNN
F 1 "GND" H 3900 7150 50  0000 C CNN
F 2 "" H 3900 7300 50  0001 C CNN
F 3 "" H 3900 7300 50  0001 C CNN
	1    3900 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C4
U 1 1 5CAFCB77
P 4600 7100
F 0 "C4" H 4625 7200 50  0000 L CNN
F 1 "100uF" H 4625 7000 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P3.50mm" H 4638 6950 50  0001 C CNN
F 3 "" H 4600 7100 50  0001 C CNN
	1    4600 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 6950 4600 6950
Wire Wire Line
	3900 6950 4250 6950
Connection ~ 4250 6950
$Comp
L Device:C C3
U 1 1 5CAFCAC6
P 4250 7100
F 0 "C3" H 4275 7200 50  0000 L CNN
F 1 "100nF" H 4275 7000 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_DIP_SPSTx01_Slide_Copal_CHS-01A_W5.08mm_P1.27mm_JPin" H 4288 6950 50  0001 C CNN
F 3 "" H 4250 7100 50  0001 C CNN
F 4 "K104K15X7RF53L2" H 1550 3600 50  0001 C CNN "part"
	1    4250 7100
	1    0    0    -1  
$EndComp
Connection ~ 3900 6950
$Comp
L Device:C C2
U 1 1 5CAFC9D1
P 3900 7100
F 0 "C2" H 3925 7200 50  0000 L CNN
F 1 "10nF" H 3925 7000 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_DIP_SPSTx01_Slide_6.7x4.1mm_W8.61mm_P2.54mm_LowProfile" H 3938 6950 50  0001 C CNN
F 3 "" H 3900 7100 50  0001 C CNN
F 4 "MCFYU5103Z5" H 1550 3600 50  0001 C CNN "part"
	1    3900 7100
	1    0    0    -1  
$EndComp
Wire Notes Line
	12000 1850 15200 1850
Wire Notes Line
	12000 800  15200 800 
Text Notes 6950 2300 0    39   ~ 0
+ avalanche
Text Notes 6950 2600 0    39   ~ 0
- avalanche (moves system ground as well)
Wire Wire Line
	9200 2350 9200 2750
Wire Wire Line
	8400 2500 8400 2750
Connection ~ 8400 2500
Wire Wire Line
	8400 2500 8550 2500
Wire Notes Line
	6750 700  11750 700 
$Comp
L Device:R R32
U 1 1 5F2E0F37
P 7650 2200
F 0 "R32" V 7730 2200 50  0000 C CNN
F 1 "10k" V 7650 2200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7580 2200 50  0001 C CNN
F 3 "" H 7650 2200 50  0001 C CNN
F 4 "MCF 0.25W 10K" H 1850 -100 50  0001 C CNN "part"
F 5 "have, C57436" H 1850 -100 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1002A50_C57436.html" H 1850 -100 50  0001 C CNN "LCSC_web"
	1    7650 2200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R33
U 1 1 5F35B4AE
P 7650 2500
F 0 "R33" V 7730 2500 50  0000 C CNN
F 1 "10k" V 7650 2500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7580 2500 50  0001 C CNN
F 3 "" H 7650 2500 50  0001 C CNN
F 4 "MCF 0.25W 10K" H 1850 -100 50  0001 C CNN "part"
F 5 "have, C57436" H 1850 -100 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1002A50_C57436.html" H 1850 -100 50  0001 C CNN "LCSC_web"
	1    7650 2500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8450 2750 8400 2750
Connection ~ 10150 2350
Text Notes 9550 1800 0    39   ~ 0
upper precision clipper
Wire Wire Line
	9500 2350 10150 2350
Wire Wire Line
	3850 8100 4100 8100
Connection ~ 4100 8100
Wire Wire Line
	5000 7700 5300 7700
Wire Wire Line
	5300 7700 5300 7850
Connection ~ 5300 7700
Wire Wire Line
	5300 7700 5650 7700
Wire Wire Line
	5300 8150 5300 8250
Text Notes 12250 800  0    39   ~ 0
Popular form factors: TO-92, SOT-223, SOT-23-5
$Comp
L Device:D_Zener D7
U 1 1 5F912B44
P 7150 6750
F 0 "D7" V 7104 6829 50  0000 L CNN
F 1 "2-3V" V 7195 6829 50  0000 L CNN
F 2 "Diode_SMD:D_MiniMELF_Handsoldering" H 7150 6750 50  0001 C CNN
F 3 "~" H 7150 6750 50  0001 C CNN
F 4 "MMBZ5223B" V 7150 6750 50  0001 C CNN "part"
	1    7150 6750
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 6600 7150 6600
Connection ~ 7150 6600
$Comp
L Device:R R42
U 1 1 5F93E166
P 9350 2350
F 0 "R42" V 9430 2350 50  0000 C CNN
F 1 "1k" V 9350 2350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9280 2350 50  0001 C CNN
F 3 "" H 9350 2350 50  0001 C CNN
F 4 "MCF 0.25W 1K" H 1850 -100 50  0001 C CNN "part"
F 5 "have, C57435" H 1850 -100 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1001A50_C57435.html" H 1850 -100 50  0001 C CNN "LCSC_web"
	1    9350 2350
	0    -1   -1   0   
$EndComp
Connection ~ 9500 2350
Wire Wire Line
	4600 7250 4250 7250
Wire Wire Line
	4250 7250 3900 7250
Connection ~ 4250 7250
Wire Wire Line
	4600 6950 5000 6950
Wire Wire Line
	5000 6950 5000 7350
Connection ~ 4600 6950
Connection ~ 5000 7350
Wire Wire Line
	3900 7250 3900 7300
Connection ~ 3900 7250
Wire Notes Line
	5900 8800 5900 6700
Wire Notes Line
	3650 6700 3650 8800
Wire Wire Line
	4650 6050 5400 6050
Wire Wire Line
	4650 6350 5400 6350
$Comp
L Device:D_Schottky_x2_Serial_ACK D10
U 1 1 5FC454A3
P 7900 1600
F 0 "D10" H 7900 1816 50  0000 C CNN
F 1 "SDMP0340LST" H 7900 1725 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-416" H 7900 1600 50  0001 C CNN
F 3 "~" H 7900 1600 50  0001 C CNN
	1    7900 1600
	0    -1   -1   0   
$EndComp
Wire Notes Line
	3650 4500 3650 5700
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 5E966DAA
P 3900 5450
F 0 "J3" H 4008 5631 50  0000 C CNN
F 1 "conn_smallwire" H 4008 5540 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 3900 5450 50  0001 C CNN
F 3 "~" H 3900 5450 50  0001 C CNN
	1    3900 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR041
U 1 1 5E9663EA
P 5700 5450
F 0 "#PWR041" H 5700 5250 50  0001 C CNN
F 1 "GNDPWR" H 5704 5296 50  0000 C CNN
F 2 "" H 5700 5400 50  0001 C CNN
F 3 "" H 5700 5400 50  0001 C CNN
	1    5700 5450
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP1
U 1 1 5E9352D0
P 4800 5450
F 0 "JP1" H 4800 5655 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 4800 5564 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 4800 5450 50  0001 C CNN
F 3 "~" H 4800 5450 50  0001 C CNN
F 4 "n/a" H 1550 3500 50  0001 C CNN "part"
	1    4800 5450
	1    0    0    -1  
$EndComp
Wire Notes Line
	5550 5800 5550 6600
Wire Notes Line
	5550 6600 3650 6600
Wire Wire Line
	4100 5450 4350 5450
$Comp
L power:-BATT #PWR034
U 1 1 5CC5F103
P 4350 5450
F 0 "#PWR034" H 4350 5300 50  0001 C CNN
F 1 "-BATT" H 4365 5623 50  0000 C CNN
F 2 "" H 4350 5450 50  0001 C CNN
F 3 "" H 4350 5450 50  0001 C CNN
	1    4350 5450
	-1   0    0    1   
$EndComp
Connection ~ 4350 5450
Wire Wire Line
	4350 5450 4650 5450
Connection ~ 4650 6350
Wire Wire Line
	4950 5450 5700 5450
$Comp
L Device:D_Schottky_x2_Serial_ACK D14
U 1 1 5CD445E6
P 8450 1600
F 0 "D14" H 8450 1816 50  0000 C CNN
F 1 "SDMP0340LST" H 8450 1725 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-416" H 8450 1600 50  0001 C CNN
F 3 "~" H 8450 1600 50  0001 C CNN
	1    8450 1600
	0    1    -1   0   
$EndComp
Connection ~ 8450 1300
Wire Wire Line
	8150 1600 8100 1600
Wire Wire Line
	7900 1300 8450 1300
$Comp
L Device:C C9
U 1 1 5CDE3FCA
P 14850 9350
F 0 "C9" H 14965 9396 50  0000 L CNN
F 1 "C" H 14965 9305 50  0000 L CNN
F 2 "" H 14888 9200 50  0001 C CNN
F 3 "~" H 14850 9350 50  0001 C CNN
	1    14850 9350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C12
U 1 1 5CDE4FC2
P 15600 9350
F 0 "C12" H 15350 9400 50  0000 L CNN
F 1 "C" H 15350 9300 50  0000 L CNN
F 2 "" H 15638 9200 50  0001 C CNN
F 3 "~" H 15600 9350 50  0001 C CNN
	1    15600 9350
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5CDE629F
P 15250 9200
F 0 "Y1" H 15250 9468 50  0000 C CNN
F 1 "8 MHz Crystal" H 15250 9377 50  0000 C CNN
F 2 "" H 15250 9200 50  0001 C CNN
F 3 "~" H 15250 9200 50  0001 C CNN
	1    15250 9200
	1    0    0    -1  
$EndComp
Text Label 14850 8700 1    39   ~ 0
osc_in
Text Label 15600 8750 1    39   ~ 0
osc_out
Wire Wire Line
	15600 8500 15600 8850
Wire Wire Line
	15100 9200 14850 9200
Wire Wire Line
	15400 9200 15600 9200
Wire Wire Line
	15600 9500 15250 9500
$Comp
L power:GND #PWR066
U 1 1 5CFA612F
P 15250 9500
F 0 "#PWR066" H 15250 9250 50  0001 C CNN
F 1 "GND" H 15255 9327 50  0000 C CNN
F 2 "" H 15250 9500 50  0001 C CNN
F 3 "" H 15250 9500 50  0001 C CNN
	1    15250 9500
	1    0    0    -1  
$EndComp
Connection ~ 15250 9500
Wire Wire Line
	15250 9500 14850 9500
Wire Wire Line
	14850 8500 14850 8850
Wire Notes Line
	15900 9750 14600 9750
$Comp
L Jumper:SolderJumper_2_Bridged JP4
U 1 1 5D11F0B4
P 15600 9000
F 0 "JP4" H 15600 9205 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 15600 9114 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 15600 9000 50  0001 C CNN
F 3 "~" H 15600 9000 50  0001 C CNN
F 4 "n/a" H 12350 7050 50  0001 C CNN "part"
	1    15600 9000
	0    1    -1   0   
$EndComp
$Comp
L Jumper:SolderJumper_2_Bridged JP3
U 1 1 5D12E431
P 14850 9000
F 0 "JP3" H 14850 9205 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 14850 9114 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 14850 9000 50  0001 C CNN
F 3 "~" H 14850 9000 50  0001 C CNN
F 4 "n/a" H 11600 7050 50  0001 C CNN "part"
	1    14850 9000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	14850 9150 14850 9200
Connection ~ 14850 9200
Wire Wire Line
	15600 9150 15600 9200
Connection ~ 15600 9200
Text Notes 6750 8800 0    39   ~ 0
- MOSFET power sharing: Nexperia appnote AN11599\n- MOSFET turn on/off times: Vishay AN608A\n- replace 10 bigger fets with 30-40 smaller ones? \n  (much better combined RDSon per buck)\n\n- simultaneous charging:\n  weld energy: e.g. 1500A * 12V * 20ms -> 360J\n  IMAX B6 50W -> 7.2s to recharge between welds (in CC phase)\n  add another shunt to sense charge current? \n  -> No need, IMAX b6 has separate connection to PC.\n\n- Add USB? Connector only, for future software work? (unlikely)\n\n- Add UART optocouplers? Not necessary.\n\n\n- mosfet array turn-on time (IRFR7446, 100ohm Rg): 250ns
$Comp
L Regulator_Linear:AP2204K-3.3 U3
U 1 1 5CCEF8DF
P 12950 1300
F 0 "U3" H 12950 1642 50  0000 C CNN
F 1 "AP2204K-3.3" H 12950 1551 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 12950 1625 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/AP2204.pdf" H 12950 1400 50  0001 C CNN
F 4 "AP2204K-3.3TRG1" H 1800 0   50  0001 C CNN "part"
F 5 "C112032" H 1800 0   50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Low-Dropout-Regulators-LDO_DIODES_AP2204K-3-3TRG1_AP2204K-3-3TRG1_C112032.html" H 1800 0   50  0001 C CNN "LCSC_web"
	1    12950 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	12950 1600 13300 1600
Wire Wire Line
	12650 1300 12600 1300
Wire Wire Line
	12600 1300 12600 1200
Connection ~ 12600 1200
Wire Wire Line
	12600 1200 12650 1200
Text Notes 8250 2200 1    39   ~ 0
cca 370mV fwd drop @ 1mA
Connection ~ 9650 6850
$Comp
L Device:R R44
U 1 1 5CD1E746
P 9500 6850
F 0 "R44" V 9580 6850 50  0000 C CNN
F 1 "1k" V 9500 6850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9430 6850 50  0001 C CNN
F 3 "" H 9500 6850 50  0001 C CNN
F 4 "MCF 0.25W 1K" H 1850 2850 50  0001 C CNN "part"
F 5 "have, C57435" H 1850 2850 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1001A50_C57435.html" H 1850 2850 50  0001 C CNN "LCSC_web"
	1    9500 6850
	0    -1   1    0   
$EndComp
Wire Wire Line
	9350 6850 9250 6850
Text Notes 8700 7050 0    39   ~ 0
foot switch cable\ndischarge resistor\n1.2 mA loss
Wire Wire Line
	8750 2750 9200 2750
$Comp
L Device:R R38
U 1 1 5F36FA23
P 8600 2750
F 0 "R38" V 8680 2750 50  0000 C CNN
F 1 "100k" V 8600 2750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8530 2750 50  0001 C CNN
F 3 "" H 8600 2750 50  0001 C CNN
F 4 "MCRE000061" H 1850 -100 50  0001 C CNN "part"
F 5 "have, C58687" H 1850 -100 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W8F1003A50_C58687.html" H 1850 -100 50  0001 C CNN "LCSC_web"
	1    8600 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R30
U 1 1 5CAD28AA
P 7100 6450
F 0 "R30" V 7180 6450 50  0000 C CNN
F 1 "330k" V 7100 6450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7030 6450 50  0001 C CNN
F 3 "" H 7100 6450 50  0001 C CNN
F 4 "n/a" H 2050 2350 50  0001 C CNN "part"
F 5 "have, C58651" H 2050 2350 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F3303A50_C58651.html" H 2050 2350 50  0001 C CNN "LCSC_web"
	1    7100 6450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R31
U 1 1 5CF94543
P 7400 6450
F 0 "R31" V 7480 6450 50  0000 C CNN
F 1 "330k" V 7400 6450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7330 6450 50  0001 C CNN
F 3 "" H 7400 6450 50  0001 C CNN
F 4 "n/a" H 2350 2350 50  0001 C CNN "part"
F 5 "have, C58651" H 2350 2350 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F3303A50_C58651.html" H 2350 2350 50  0001 C CNN "LCSC_web"
	1    7400 6450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R36
U 1 1 5CFB353F
P 7850 6450
F 0 "R36" V 7930 6450 50  0000 C CNN
F 1 "100k" V 7850 6450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7780 6450 50  0001 C CNN
F 3 "" H 7850 6450 50  0001 C CNN
F 4 "MCRE000061" H 1100 3600 50  0001 C CNN "part"
F 5 "have, C58687" H 1100 3600 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W8F1003A50_C58687.html" H 1100 3600 50  0001 C CNN "LCSC_web"
	1    7850 6450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R37
U 1 1 5CFE1592
P 8200 6450
F 0 "R37" V 8280 6450 50  0000 C CNN
F 1 "100k" V 8200 6450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8130 6450 50  0001 C CNN
F 3 "" H 8200 6450 50  0001 C CNN
F 4 "MCRE000061" H 1450 3600 50  0001 C CNN "part"
F 5 "have, C58687" H 1450 3600 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W8F1003A50_C58687.html" H 1450 3600 50  0001 C CNN "LCSC_web"
	1    8200 6450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8400 6450 8350 6450
Wire Wire Line
	8050 6450 8000 6450
Wire Wire Line
	6900 6450 6950 6450
Wire Wire Line
	7550 6450 7600 6450
Wire Wire Line
	7150 6600 7600 6600
Wire Wire Line
	7600 6600 7600 6450
Connection ~ 7600 6600
Wire Wire Line
	7600 6600 8000 6600
Connection ~ 7600 6450
Wire Wire Line
	7600 6450 7700 6450
Wire Notes Line
	6750 7550 8550 7550
Wire Notes Line
	8550 6100 8550 7550
Wire Notes Line
	6750 6100 6750 7550
$Comp
L stm32-pindef:STM32F030F4Px_u U4
U 3 1 5CDFC663
P 15300 6500
F 0 "U4" H 15800 6750 50  0000 C CNN
F 1 "STM32F030F4Px_u" H 15050 6750 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 15457 6775 50  0001 C CIN
F 3 "" H 15300 6500 50  0000 C CNN
F 4 "C32908" H 50  -1200 50  0001 C CNN "LCSC"
F 5 "https://lcsc.com/product-detail/ST-Microelectronics_STMicroelectronics_STM32F030F4P6_STM32F030F4P6_C32908.html" H 50  -1200 50  0001 C CNN "LCSC_web"
	3    15300 6500
	-1   0    0    -1  
$EndComp
$Comp
L stm32-pindef:STM32F030F4Px_u U4
U 2 1 5CDFA98E
P 14900 6100
F 0 "U4" H 15850 6250 50  0000 C CNN
F 1 "STM32F030F4Px_u" H 14250 6250 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 15057 6275 50  0001 C CIN
F 3 "" H 14900 6100 50  0000 C CNN
F 4 "C32908" H -350 -1000 50  0001 C CNN "LCSC"
F 5 "https://lcsc.com/product-detail/ST-Microelectronics_STMicroelectronics_STM32F030F4P6_STM32F030F4P6_C32908.html" H -350 -1000 50  0001 C CNN "LCSC_web"
	2    14900 6100
	-1   0    0    -1  
$EndComp
$Comp
L stm32-pindef:STM32F030F4Px_u U4
U 5 1 5CDF62DE
P 15800 7300
F 0 "U4" H 15900 7450 50  0000 R CNN
F 1 "STM32F030F4Px_u" H 16400 7150 50  0000 R CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 15932 7475 50  0001 C CIN
F 3 "" H 15800 7300 50  0000 C CNN
F 4 "C32908" H 150 2100 50  0001 C CNN "LCSC"
F 5 "https://lcsc.com/product-detail/ST-Microelectronics_STMicroelectronics_STM32F030F4P6_STM32F030F4P6_C32908.html" H 150 2100 50  0001 C CNN "LCSC_web"
	5    15800 7300
	-1   0    0    -1  
$EndComp
$Comp
L stm32-pindef:STM32F030F4Px_u U4
U 6 1 5CDF4BBE
P 15700 7800
F 0 "U4" H 15850 8050 50  0000 C CNN
F 1 "STM32F030F4Px_u" H 15850 7650 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 15732 8575 50  0001 C CIN
F 3 "" H 15700 7800 50  0000 C CNN
F 4 "C32908" H -400 3650 50  0001 C CNN "LCSC"
F 5 "https://lcsc.com/product-detail/ST-Microelectronics_STMicroelectronics_STM32F030F4P6_STM32F030F4P6_C32908.html" H -400 3650 50  0001 C CNN "LCSC_web"
	6    15700 7800
	-1   0    0    -1  
$EndComp
$Comp
L stm32-pindef:STM32F030F4Px_u U4
U 4 1 5CDF7AB3
P 15700 6900
F 0 "U4" H 15900 7150 50  0000 R CNN
F 1 "STM32F030F4Px_u" H 16200 6750 50  0000 R CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 15857 7175 50  0001 C CIN
F 3 "" H 15700 6900 50  0000 C CNN
F 4 "C32908" H 200 1100 50  0001 C CNN "LCSC"
F 5 "https://lcsc.com/product-detail/ST-Microelectronics_STMicroelectronics_STM32F030F4P6_STM32F030F4P6_C32908.html" H 200 1100 50  0001 C CNN "LCSC_web"
	4    15700 6900
	-1   0    0    -1  
$EndComp
$Comp
L stm32-pindef:STM32F030F4Px_u U4
U 1 1 5CDF8E03
P 14500 5200
F 0 "U4" H 15850 5950 50  0000 C CNN
F 1 "STM32F030F4Px_u" H 13450 4550 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 14550 4550 50  0000 C CIN
F 3 "" H 14500 5200 50  0000 C CNN
F 4 "C32908" H -700 -1300 50  0001 C CNN "LCSC"
F 5 "https://lcsc.com/product-detail/ST-Microelectronics_STMicroelectronics_STM32F030F4P6_STM32F030F4P6_C32908.html" H -700 -1300 50  0001 C CNN "LCSC_web"
	1    14500 5200
	-1   0    0    -1  
$EndComp
Text Label 14150 6400 0    39   ~ 0
osc_in
Text Label 14150 6500 0    39   ~ 0
osc_out
$Comp
L power:GND #PWR080
U 1 1 5D460C95
P 14950 7300
F 0 "#PWR080" H 14950 7050 50  0001 C CNN
F 1 "GND" H 14955 7127 50  0000 C CNN
F 2 "" H 14950 7300 50  0001 C CNN
F 3 "" H 14950 7300 50  0001 C CNN
	1    14950 7300
	-1   0    0    -1  
$EndComp
$Comp
L power:GNDA #PWR078
U 1 1 5D460C9B
P 14650 7300
F 0 "#PWR078" H 14650 7050 50  0001 C CNN
F 1 "GNDA" H 14655 7127 50  0000 C CNN
F 2 "" H 14650 7300 50  0001 C CNN
F 3 "" H 14650 7300 50  0001 C CNN
	1    14650 7300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14650 7300 14950 7300
Connection ~ 14950 7300
Wire Wire Line
	14950 7300 15400 7300
Wire Wire Line
	14850 7900 15100 7900
Wire Wire Line
	15100 7900 15100 7800
Wire Wire Line
	15100 7800 15200 7800
Text Label 15050 6900 2    39   ~ 0
mcu_reset
Text Label 15050 6800 2    39   ~ 0
mcu_boot0
Wire Wire Line
	15200 6800 14800 6800
Wire Wire Line
	14800 6900 15200 6900
Text Label 12450 4900 0    39   ~ 0
USART_RX
Text Label 12450 4800 0    39   ~ 0
USART_TX
Wire Wire Line
	12450 4800 12800 4800
Wire Wire Line
	12450 4900 12800 4900
Text Label 12400 4700 0    39   ~ 0
U_batt
Text Label 12400 4600 0    39   ~ 0
U_rdson
Text Notes 12050 4500 0    39   ~ 0
Note: add grounds in the\nDMA ADC pin series to\ndrain the ADC capacitor?
Wire Wire Line
	12400 4700 12800 4700
Wire Wire Line
	12400 4600 12800 4600
Text Label 12450 5700 0    39   ~ 0
SWCLK
Text Label 12450 5600 0    39   ~ 0
SWDIO
Text Label 13250 6100 0    39   ~ 0
gate_drv
Wire Wire Line
	13600 6100 13250 6100
Text Label 12100 5700 0    39   ~ 0
foot_sense
Text Notes 15050 8650 0    79   ~ 0
Timing
Wire Wire Line
	12450 5600 12800 5600
Wire Notes Line
	13650 6650 14400 6650
Wire Notes Line
	14400 6650 14400 7500
Wire Notes Line
	14400 7500 13650 7500
Wire Notes Line
	13650 7500 13650 6650
Wire Wire Line
	12000 3400 12350 3400
Wire Notes Line
	13950 2700 11950 2700
Wire Notes Line
	11950 4150 13950 4150
Wire Notes Line
	13950 2700 13950 4150
Wire Notes Line
	15950 2700 14050 2700
Wire Notes Line
	14050 4350 15950 4350
Text Label 10450 5300 0    59   ~ 0
U_aux
Wire Wire Line
	8600 5450 8600 5400
Text Notes 7000 4150 0    39   ~ 0
Electrical over stress\nprotection
Wire Wire Line
	8450 5450 8450 5700
Connection ~ 8450 5450
Wire Wire Line
	8450 5450 8600 5450
$Comp
L Device:R R34
U 1 1 5D98B39A
P 7700 5150
F 0 "R34" V 7780 5150 50  0000 C CNN
F 1 "10k" V 7700 5150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 5150 50  0001 C CNN
F 3 "" H 7700 5150 50  0001 C CNN
F 4 "MCF 0.25W 10K" H 1900 2850 50  0001 C CNN "part"
F 5 "have, C57436" H 1900 2850 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1002A50_C57436.html" H 1900 2850 50  0001 C CNN "LCSC_web"
	1    7700 5150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R35
U 1 1 5D98B3A3
P 7700 5450
F 0 "R35" V 7780 5450 50  0000 C CNN
F 1 "10k" V 7700 5450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 7630 5450 50  0001 C CNN
F 3 "" H 7700 5450 50  0001 C CNN
F 4 "MCF 0.25W 10K" H 1900 2850 50  0001 C CNN "part"
F 5 "have, C57436" H 1900 2850 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1002A50_C57436.html" H 1900 2850 50  0001 C CNN "LCSC_web"
	1    7700 5450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8500 5700 8450 5700
$Comp
L Diode:1.5KExxA D9
U 1 1 5D98B3B7
P 10400 3350
F 0 "D9" H 10400 3450 50  0000 C CNN
F 1 "TVS SMDJ13A" H 10400 3250 50  0000 C CNN
F 2 "Diode_SMD:D_SMC" H 10400 3350 50  0001 C CNN
F 3 "" H 10400 3350 50  0000 C CNN
	1    10400 3350
	0    -1   1    0   
$EndComp
Wire Wire Line
	8800 5700 9250 5700
$Comp
L Device:R R39
U 1 1 5D98B3D9
P 8650 5700
F 0 "R39" V 8730 5700 50  0000 C CNN
F 1 "100k" V 8650 5700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8580 5700 50  0001 C CNN
F 3 "" H 8650 5700 50  0001 C CNN
F 4 "MCRE000061" H 1900 2850 50  0001 C CNN "part"
F 5 "have, C58687" H 1900 2850 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W8F1003A50_C58687.html" H 1900 2850 50  0001 C CNN "LCSC_web"
	1    8650 5700
	0    -1   -1   0   
$EndComp
Text Notes 6800 3900 0    79   ~ 0
Auxillary current sense amplifier (from batt+ to weld+?)
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 5DA0A336
P 7050 5150
F 0 "J5" H 7158 5331 50  0000 C CNN
F 1 "diff aux header" H 7158 5240 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Horizontal" H 7050 5150 50  0001 C CNN
F 3 "~" H 7050 5150 50  0001 C CNN
	1    7050 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 5150 7250 5150
$Comp
L Jumper:SolderJumper_2_Open JP2
U 1 1 5DA98D10
P 7650 5650
F 0 "JP2" H 7650 5750 50  0000 C CNN
F 1 "SolderJumper_2_Open" H 7650 5550 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 7650 5650 50  0001 C CNN
F 3 "~" H 7650 5650 50  0001 C CNN
	1    7650 5650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR050
U 1 1 5DAAE162
P 8100 5650
F 0 "#PWR050" H 8100 5400 50  0001 C CNN
F 1 "GND" H 8105 5477 50  0000 C CNN
F 2 "" H 8100 5650 50  0001 C CNN
F 3 "" H 8100 5650 50  0001 C CNN
	1    8100 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 5650 7800 5650
Wire Wire Line
	7500 5650 7150 5650
Wire Wire Line
	7150 5650 7150 5450
Wire Wire Line
	7150 5450 7250 5450
Wire Wire Line
	7250 5250 7250 5450
Connection ~ 7250 5450
Wire Wire Line
	7250 5450 7550 5450
$Comp
L Device:Opamp_Quad_Generic U2
U 5 1 5F28B9C6
P 11200 3350
F 0 "U2" H 11158 3396 50  0000 L CNN
F 1 "quad opamp" H 11158 3305 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 11200 3350 50  0001 C CNN
F 3 "~" H 11200 3350 50  0001 C CNN
F 4 "LM224ADT" H 1800 1450 50  0001 C CNN "part"
F 5 "LM224ADT" H 1800 1450 50  0001 C CNN "arrow"
	5    11200 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Quad_Generic U2
U 2 1 5F1CA6E3
P 9800 2050
F 0 "U2" H 9800 2250 50  0000 C CNN
F 1 "quad opamp" H 9850 1850 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 9800 2050 50  0001 C CNN
F 3 "~" H 9800 2050 50  0001 C CNN
F 4 "LM224ADT" H 1850 -100 50  0001 C CNN "part"
F 5 "LM224ADT" H 1850 -100 50  0001 C CNN "arrow"
	2    9800 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Quad_Generic U2
U 1 1 5F1C8D69
P 8850 2350
F 0 "U2" H 8850 2550 50  0000 C CNN
F 1 "quad opamp" H 8900 2150 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8850 2350 50  0001 C CNN
F 3 "~" H 8850 2350 50  0001 C CNN
F 4 "LM224ADT" H 1850 -100 50  0001 C CNN "part"
F 5 "LM224ADT" H 1850 -100 50  0001 C CNN "arrow"
	1    8850 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5CD62BD6
P 900 1050
F 0 "R1" V 800 1050 50  0000 C CNN
F 1 "65" V 900 1050 50  0000 C CNN
F 2 "" V 830 1050 50  0001 C CNN
F 3 "~" H 900 1050 50  0001 C CNN
	1    900  1050
	0    1    1    0   
$EndComp
Wire Wire Line
	1100 1250 1350 1250
$Comp
L power:GNDPWR #PWR01
U 1 1 5CD3068F
P 1100 1250
F 0 "#PWR01" H 1100 1050 50  0001 C CNN
F 1 "GNDPWR" H 1104 1096 50  0000 C CNN
F 2 "" H 1100 1200 50  0001 C CNN
F 3 "" H 1100 1200 50  0001 C CNN
	1    1100 1250
	1    0    0    -1  
$EndComp
Text Label 2050 850  0    60   ~ 0
weld-
$Comp
L Transistor_FET:IRF540N Q1
U 1 1 5CD30671
P 1250 1050
F 0 "Q1" H 1500 1125 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 1050 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 975 50  0001 L CIN
F 3 "" H 1250 1050 50  0001 L CNN
	1    1250 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 1800 1350 1800
$Comp
L power:GNDPWR #PWR02
U 1 1 5CE12EFB
P 1100 1800
F 0 "#PWR02" H 1100 1600 50  0001 C CNN
F 1 "GNDPWR" H 1104 1646 50  0000 C CNN
F 2 "" H 1100 1750 50  0001 C CNN
F 3 "" H 1100 1750 50  0001 C CNN
	1    1100 1800
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q2
U 1 1 5CE12F0D
P 1250 1600
F 0 "Q2" H 1500 1675 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 1600 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 1525 50  0001 L CIN
F 3 "" H 1250 1600 50  0001 L CNN
	1    1250 1600
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR035
U 1 1 5CEF90F0
P 4400 1500
F 0 "#PWR035" H 4400 1300 50  0001 C CNN
F 1 "GNDPWR" H 4404 1346 50  0000 C CNN
F 2 "" H 4400 1450 50  0001 C CNN
F 3 "" H 4400 1450 50  0001 C CNN
	1    4400 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 850  2000 1400
Wire Wire Line
	2000 850  1350 850 
Connection ~ 2000 850 
Wire Wire Line
	1350 1400 2000 1400
Connection ~ 2000 1400
Wire Wire Line
	1100 2350 1350 2350
$Comp
L power:GNDPWR #PWR03
U 1 1 5D002902
P 1100 2350
F 0 "#PWR03" H 1100 2150 50  0001 C CNN
F 1 "GNDPWR" H 1104 2196 50  0000 C CNN
F 2 "" H 1100 2300 50  0001 C CNN
F 3 "" H 1100 2300 50  0001 C CNN
	1    1100 2350
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q3
U 1 1 5D002909
P 1250 2150
F 0 "Q3" H 1500 2225 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 2150 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 2075 50  0001 L CIN
F 3 "" H 1250 2150 50  0001 L CNN
	1    1250 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 2900 1350 2900
$Comp
L power:GNDPWR #PWR04
U 1 1 5D002916
P 1100 2900
F 0 "#PWR04" H 1100 2700 50  0001 C CNN
F 1 "GNDPWR" H 1104 2746 50  0000 C CNN
F 2 "" H 1100 2850 50  0001 C CNN
F 3 "" H 1100 2850 50  0001 C CNN
	1    1100 2900
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q4
U 1 1 5D00291C
P 1250 2700
F 0 "Q4" H 1500 2775 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 2700 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 2625 50  0001 L CIN
F 3 "" H 1250 2700 50  0001 L CNN
	1    1250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 1950 2000 2500
Wire Wire Line
	2000 1950 1350 1950
Wire Wire Line
	1350 2500 2000 2500
Connection ~ 2000 2500
Wire Wire Line
	2000 1400 2000 1950
Connection ~ 2000 1950
Text Notes 1350 8650 0    60   ~ 0
U=13, R=75\nIg=U/R = 173mA
Text Label 800  8800 0    59   ~ 0
PWR_gate_drv
$Comp
L Device:R R2
U 1 1 5D094AC8
P 900 1600
F 0 "R2" V 800 1600 50  0000 C CNN
F 1 "65" V 900 1600 50  0000 C CNN
F 2 "" V 830 1600 50  0001 C CNN
F 3 "~" H 900 1600 50  0001 C CNN
	1    900  1600
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D0ACA97
P 900 2150
F 0 "R3" V 800 2150 50  0000 C CNN
F 1 "65" V 900 2150 50  0000 C CNN
F 2 "" V 830 2150 50  0001 C CNN
F 3 "~" H 900 2150 50  0001 C CNN
	1    900  2150
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5D0C48B5
P 900 2700
F 0 "R4" V 800 2700 50  0000 C CNN
F 1 "65" V 900 2700 50  0000 C CNN
F 2 "" V 830 2700 50  0001 C CNN
F 3 "~" H 900 2700 50  0001 C CNN
	1    900  2700
	0    1    1    0   
$EndComp
Connection ~ 750  2150
Wire Wire Line
	750  2150 750  2700
Wire Wire Line
	750  1050 750  1600
Connection ~ 750  1600
Wire Wire Line
	750  1600 750  2150
$Comp
L Device:R R5
U 1 1 5D1300ED
P 900 3250
F 0 "R5" V 800 3250 50  0000 C CNN
F 1 "65" V 900 3250 50  0000 C CNN
F 2 "" V 830 3250 50  0001 C CNN
F 3 "~" H 900 3250 50  0001 C CNN
	1    900  3250
	0    1    1    0   
$EndComp
Wire Wire Line
	1100 3450 1350 3450
$Comp
L power:GNDPWR #PWR05
U 1 1 5D1300F4
P 1100 3450
F 0 "#PWR05" H 1100 3250 50  0001 C CNN
F 1 "GNDPWR" H 1104 3296 50  0000 C CNN
F 2 "" H 1100 3400 50  0001 C CNN
F 3 "" H 1100 3400 50  0001 C CNN
	1    1100 3450
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q5
U 1 1 5D1300FB
P 1250 3250
F 0 "Q5" H 1500 3325 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 3250 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 3175 50  0001 L CIN
F 3 "" H 1250 3250 50  0001 L CNN
	1    1250 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 4000 1350 4000
$Comp
L power:GNDPWR #PWR06
U 1 1 5D130102
P 1100 4000
F 0 "#PWR06" H 1100 3800 50  0001 C CNN
F 1 "GNDPWR" H 1104 3846 50  0000 C CNN
F 2 "" H 1100 3950 50  0001 C CNN
F 3 "" H 1100 3950 50  0001 C CNN
	1    1100 4000
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q6
U 1 1 5D130108
P 1250 3800
F 0 "Q6" H 1500 3875 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 3800 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 3725 50  0001 L CIN
F 3 "" H 1250 3800 50  0001 L CNN
	1    1250 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 3050 2000 3600
Wire Wire Line
	2000 3050 1350 3050
Wire Wire Line
	1350 3600 2000 3600
Connection ~ 2000 3600
Wire Wire Line
	1100 4550 1350 4550
$Comp
L power:GNDPWR #PWR07
U 1 1 5D130114
P 1100 4550
F 0 "#PWR07" H 1100 4350 50  0001 C CNN
F 1 "GNDPWR" H 1104 4396 50  0000 C CNN
F 2 "" H 1100 4500 50  0001 C CNN
F 3 "" H 1100 4500 50  0001 C CNN
	1    1100 4550
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q7
U 1 1 5D13011A
P 1250 4350
F 0 "Q7" H 1500 4425 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 4350 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 4275 50  0001 L CIN
F 3 "" H 1250 4350 50  0001 L CNN
	1    1250 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 5100 1350 5100
$Comp
L power:GNDPWR #PWR08
U 1 1 5D130121
P 1100 5100
F 0 "#PWR08" H 1100 4900 50  0001 C CNN
F 1 "GNDPWR" H 1104 4946 50  0000 C CNN
F 2 "" H 1100 5050 50  0001 C CNN
F 3 "" H 1100 5050 50  0001 C CNN
	1    1100 5100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q8
U 1 1 5D130127
P 1250 4900
F 0 "Q8" H 1500 4975 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 4900 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 4825 50  0001 L CIN
F 3 "" H 1250 4900 50  0001 L CNN
	1    1250 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 4150 2000 4700
Wire Wire Line
	2000 4150 1350 4150
Wire Wire Line
	1350 4700 2000 4700
Connection ~ 2000 4700
Wire Wire Line
	2000 3600 2000 4150
Connection ~ 2000 4150
$Comp
L Device:R R6
U 1 1 5D130134
P 900 3800
F 0 "R6" V 800 3800 50  0000 C CNN
F 1 "65" V 900 3800 50  0000 C CNN
F 2 "" V 830 3800 50  0001 C CNN
F 3 "~" H 900 3800 50  0001 C CNN
	1    900  3800
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5D13013A
P 900 4350
F 0 "R7" V 800 4350 50  0000 C CNN
F 1 "65" V 900 4350 50  0000 C CNN
F 2 "" V 830 4350 50  0001 C CNN
F 3 "~" H 900 4350 50  0001 C CNN
	1    900  4350
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5D130140
P 900 4900
F 0 "R8" V 800 4900 50  0000 C CNN
F 1 "65" V 900 4900 50  0000 C CNN
F 2 "" V 830 4900 50  0001 C CNN
F 3 "~" H 900 4900 50  0001 C CNN
	1    900  4900
	0    1    1    0   
$EndComp
Connection ~ 750  4350
Wire Wire Line
	750  4350 750  4900
Wire Wire Line
	750  3250 750  3800
Connection ~ 750  3800
Wire Wire Line
	750  3800 750  4350
Wire Wire Line
	2000 2500 2000 3050
Connection ~ 2000 3050
Wire Wire Line
	750  2700 750  3250
Connection ~ 750  2700
Connection ~ 750  3250
$Comp
L Device:R R9
U 1 1 5D1AB1CA
P 900 5450
F 0 "R9" V 800 5450 50  0000 C CNN
F 1 "65" V 900 5450 50  0000 C CNN
F 2 "" V 830 5450 50  0001 C CNN
F 3 "~" H 900 5450 50  0001 C CNN
	1    900  5450
	0    1    1    0   
$EndComp
Wire Wire Line
	1100 5650 1350 5650
$Comp
L power:GNDPWR #PWR09
U 1 1 5D1AB1D1
P 1100 5650
F 0 "#PWR09" H 1100 5450 50  0001 C CNN
F 1 "GNDPWR" H 1104 5496 50  0000 C CNN
F 2 "" H 1100 5600 50  0001 C CNN
F 3 "" H 1100 5600 50  0001 C CNN
	1    1100 5650
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q9
U 1 1 5D1AB1D8
P 1250 5450
F 0 "Q9" H 1500 5525 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 5450 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 5375 50  0001 L CIN
F 3 "" H 1250 5450 50  0001 L CNN
	1    1250 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 6200 1350 6200
$Comp
L power:GNDPWR #PWR010
U 1 1 5D1AB1DF
P 1100 6200
F 0 "#PWR010" H 1100 6000 50  0001 C CNN
F 1 "GNDPWR" H 1104 6046 50  0000 C CNN
F 2 "" H 1100 6150 50  0001 C CNN
F 3 "" H 1100 6150 50  0001 C CNN
	1    1100 6200
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q10
U 1 1 5D1AB1E5
P 1250 6000
F 0 "Q10" H 1500 6075 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 6000 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 5925 50  0001 L CIN
F 3 "" H 1250 6000 50  0001 L CNN
	1    1250 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 5250 2000 5800
Wire Wire Line
	2000 5250 1350 5250
Wire Wire Line
	1350 5800 2000 5800
Connection ~ 2000 5800
Wire Wire Line
	1100 6750 1350 6750
$Comp
L power:GNDPWR #PWR011
U 1 1 5D1AB1F1
P 1100 6750
F 0 "#PWR011" H 1100 6550 50  0001 C CNN
F 1 "GNDPWR" H 1104 6596 50  0000 C CNN
F 2 "" H 1100 6700 50  0001 C CNN
F 3 "" H 1100 6700 50  0001 C CNN
	1    1100 6750
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q11
U 1 1 5D1AB1F7
P 1250 6550
F 0 "Q11" H 1500 6625 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 6550 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 6475 50  0001 L CIN
F 3 "" H 1250 6550 50  0001 L CNN
	1    1250 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 7300 1350 7300
$Comp
L power:GNDPWR #PWR012
U 1 1 5D1AB1FE
P 1100 7300
F 0 "#PWR012" H 1100 7100 50  0001 C CNN
F 1 "GNDPWR" H 1104 7146 50  0000 C CNN
F 2 "" H 1100 7250 50  0001 C CNN
F 3 "" H 1100 7250 50  0001 C CNN
	1    1100 7300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q12
U 1 1 5D1AB204
P 1250 7100
F 0 "Q12" H 1500 7175 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 7100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 7025 50  0001 L CIN
F 3 "" H 1250 7100 50  0001 L CNN
	1    1250 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 6350 2000 6900
Wire Wire Line
	2000 6350 1350 6350
Wire Wire Line
	1350 6900 2000 6900
Connection ~ 2000 6900
Wire Wire Line
	2000 5800 2000 6350
Connection ~ 2000 6350
$Comp
L Device:R R10
U 1 1 5D1AB210
P 900 6000
F 0 "R10" V 800 6000 50  0000 C CNN
F 1 "65" V 900 6000 50  0000 C CNN
F 2 "" V 830 6000 50  0001 C CNN
F 3 "~" H 900 6000 50  0001 C CNN
	1    900  6000
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5D1AB216
P 900 6550
F 0 "R11" V 800 6550 50  0000 C CNN
F 1 "65" V 900 6550 50  0000 C CNN
F 2 "" V 830 6550 50  0001 C CNN
F 3 "~" H 900 6550 50  0001 C CNN
	1    900  6550
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5D1AB21C
P 900 7100
F 0 "R12" V 800 7100 50  0000 C CNN
F 1 "65" V 900 7100 50  0000 C CNN
F 2 "" V 830 7100 50  0001 C CNN
F 3 "~" H 900 7100 50  0001 C CNN
	1    900  7100
	0    1    1    0   
$EndComp
Connection ~ 750  6550
Wire Wire Line
	750  6550 750  7100
Wire Wire Line
	750  5450 750  6000
Connection ~ 750  6000
Wire Wire Line
	750  6000 750  6550
$Comp
L Device:R R13
U 1 1 5D1AB227
P 900 7650
F 0 "R13" V 800 7650 50  0000 C CNN
F 1 "65" V 900 7650 50  0000 C CNN
F 2 "" V 830 7650 50  0001 C CNN
F 3 "~" H 900 7650 50  0001 C CNN
	1    900  7650
	0    1    1    0   
$EndComp
Wire Wire Line
	1100 7850 1350 7850
$Comp
L power:GNDPWR #PWR013
U 1 1 5D1AB22E
P 1100 7850
F 0 "#PWR013" H 1100 7650 50  0001 C CNN
F 1 "GNDPWR" H 1104 7696 50  0000 C CNN
F 2 "" H 1100 7800 50  0001 C CNN
F 3 "" H 1100 7800 50  0001 C CNN
	1    1100 7850
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q13
U 1 1 5D1AB234
P 1250 7650
F 0 "Q13" H 1500 7725 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 7650 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 7575 50  0001 L CIN
F 3 "" H 1250 7650 50  0001 L CNN
	1    1250 7650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 8400 1350 8400
$Comp
L power:GNDPWR #PWR014
U 1 1 5D1AB23B
P 1100 8400
F 0 "#PWR014" H 1100 8200 50  0001 C CNN
F 1 "GNDPWR" H 1104 8246 50  0000 C CNN
F 2 "" H 1100 8350 50  0001 C CNN
F 3 "" H 1100 8350 50  0001 C CNN
	1    1100 8400
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q14
U 1 1 5D1AB241
P 1250 8200
F 0 "Q14" H 1500 8275 50  0000 L CNN
F 1 "irfR7446pbf" H 1500 8200 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 1500 8125 50  0001 L CIN
F 3 "" H 1250 8200 50  0001 L CNN
	1    1250 8200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 7450 2000 8000
Wire Wire Line
	2000 7450 1350 7450
Wire Wire Line
	1350 8000 2000 8000
$Comp
L Device:R R14
U 1 1 5D1AB26C
P 900 8200
F 0 "R14" V 800 8200 50  0000 C CNN
F 1 "65" V 900 8200 50  0000 C CNN
F 2 "" V 830 8200 50  0001 C CNN
F 3 "~" H 900 8200 50  0001 C CNN
	1    900  8200
	0    1    1    0   
$EndComp
Wire Wire Line
	750  7650 750  8200
Wire Wire Line
	2000 6900 2000 7450
Connection ~ 2000 7450
Wire Wire Line
	750  7100 750  7650
Connection ~ 750  7100
Connection ~ 750  7650
Wire Wire Line
	2000 4700 2000 5250
Connection ~ 2000 5250
Wire Wire Line
	750  4900 750  5450
Connection ~ 750  4900
Connection ~ 750  5450
$Comp
L Device:R R15
U 1 1 5D2F1C2D
P 2350 1050
F 0 "R15" V 2250 1050 50  0000 C CNN
F 1 "65" V 2350 1050 50  0000 C CNN
F 2 "" V 2280 1050 50  0001 C CNN
F 3 "~" H 2350 1050 50  0001 C CNN
	1    2350 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 1250 2800 1250
$Comp
L power:GNDPWR #PWR015
U 1 1 5D2F1C34
P 2550 1250
F 0 "#PWR015" H 2550 1050 50  0001 C CNN
F 1 "GNDPWR" H 2554 1096 50  0000 C CNN
F 2 "" H 2550 1200 50  0001 C CNN
F 3 "" H 2550 1200 50  0001 C CNN
	1    2550 1250
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q15
U 1 1 5D2F1C3C
P 2700 1050
F 0 "Q15" H 2950 1125 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 1050 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 975 50  0001 L CIN
F 3 "" H 2700 1050 50  0001 L CNN
	1    2700 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 1800 2800 1800
$Comp
L power:GNDPWR #PWR016
U 1 1 5D2F1C43
P 2550 1800
F 0 "#PWR016" H 2550 1600 50  0001 C CNN
F 1 "GNDPWR" H 2554 1646 50  0000 C CNN
F 2 "" H 2550 1750 50  0001 C CNN
F 3 "" H 2550 1750 50  0001 C CNN
	1    2550 1800
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q16
U 1 1 5D2F1C49
P 2700 1600
F 0 "Q16" H 2950 1675 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 1600 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 1525 50  0001 L CIN
F 3 "" H 2700 1600 50  0001 L CNN
	1    2700 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 850  3450 1400
Wire Wire Line
	3450 850  2800 850 
Wire Wire Line
	2800 1400 3450 1400
Connection ~ 3450 1400
Wire Wire Line
	2550 2350 2800 2350
$Comp
L power:GNDPWR #PWR017
U 1 1 5D2F1C55
P 2550 2350
F 0 "#PWR017" H 2550 2150 50  0001 C CNN
F 1 "GNDPWR" H 2554 2196 50  0000 C CNN
F 2 "" H 2550 2300 50  0001 C CNN
F 3 "" H 2550 2300 50  0001 C CNN
	1    2550 2350
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q17
U 1 1 5D2F1C5B
P 2700 2150
F 0 "Q17" H 2950 2225 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 2150 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 2075 50  0001 L CIN
F 3 "" H 2700 2150 50  0001 L CNN
	1    2700 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2900 2800 2900
$Comp
L power:GNDPWR #PWR018
U 1 1 5D2F1C62
P 2550 2900
F 0 "#PWR018" H 2550 2700 50  0001 C CNN
F 1 "GNDPWR" H 2554 2746 50  0000 C CNN
F 2 "" H 2550 2850 50  0001 C CNN
F 3 "" H 2550 2850 50  0001 C CNN
	1    2550 2900
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q18
U 1 1 5D2F1C68
P 2700 2700
F 0 "Q18" H 2950 2775 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 2700 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 2625 50  0001 L CIN
F 3 "" H 2700 2700 50  0001 L CNN
	1    2700 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1950 3450 2500
Wire Wire Line
	3450 1950 2800 1950
Wire Wire Line
	2800 2500 3450 2500
Connection ~ 3450 2500
Wire Wire Line
	3450 1400 3450 1950
Connection ~ 3450 1950
$Comp
L Device:R R16
U 1 1 5D2F1C74
P 2350 1600
F 0 "R16" V 2250 1600 50  0000 C CNN
F 1 "65" V 2350 1600 50  0000 C CNN
F 2 "" V 2280 1600 50  0001 C CNN
F 3 "~" H 2350 1600 50  0001 C CNN
	1    2350 1600
	0    1    1    0   
$EndComp
$Comp
L Device:R R17
U 1 1 5D2F1C7A
P 2350 2150
F 0 "R17" V 2250 2150 50  0000 C CNN
F 1 "65" V 2350 2150 50  0000 C CNN
F 2 "" V 2280 2150 50  0001 C CNN
F 3 "~" H 2350 2150 50  0001 C CNN
	1    2350 2150
	0    1    1    0   
$EndComp
$Comp
L Device:R R18
U 1 1 5D2F1C80
P 2350 2700
F 0 "R18" V 2250 2700 50  0000 C CNN
F 1 "65" V 2350 2700 50  0000 C CNN
F 2 "" V 2280 2700 50  0001 C CNN
F 3 "~" H 2350 2700 50  0001 C CNN
	1    2350 2700
	0    1    1    0   
$EndComp
Connection ~ 2200 2150
Wire Wire Line
	2200 2150 2200 2700
Wire Wire Line
	2200 1050 2200 1600
Connection ~ 2200 1600
Wire Wire Line
	2200 1600 2200 2150
$Comp
L Device:R R19
U 1 1 5D2F1C8B
P 2350 3250
F 0 "R19" V 2250 3250 50  0000 C CNN
F 1 "65" V 2350 3250 50  0000 C CNN
F 2 "" V 2280 3250 50  0001 C CNN
F 3 "~" H 2350 3250 50  0001 C CNN
	1    2350 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 3450 2800 3450
$Comp
L power:GNDPWR #PWR019
U 1 1 5D2F1C92
P 2550 3450
F 0 "#PWR019" H 2550 3250 50  0001 C CNN
F 1 "GNDPWR" H 2554 3296 50  0000 C CNN
F 2 "" H 2550 3400 50  0001 C CNN
F 3 "" H 2550 3400 50  0001 C CNN
	1    2550 3450
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q19
U 1 1 5D2F1C98
P 2700 3250
F 0 "Q19" H 2950 3325 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 3250 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 3175 50  0001 L CIN
F 3 "" H 2700 3250 50  0001 L CNN
	1    2700 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 4000 2800 4000
$Comp
L power:GNDPWR #PWR020
U 1 1 5D2F1C9F
P 2550 4000
F 0 "#PWR020" H 2550 3800 50  0001 C CNN
F 1 "GNDPWR" H 2554 3846 50  0000 C CNN
F 2 "" H 2550 3950 50  0001 C CNN
F 3 "" H 2550 3950 50  0001 C CNN
	1    2550 4000
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q20
U 1 1 5D2F1CA5
P 2700 3800
F 0 "Q20" H 2950 3875 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 3800 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 3725 50  0001 L CIN
F 3 "" H 2700 3800 50  0001 L CNN
	1    2700 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 3050 3450 3600
Wire Wire Line
	3450 3050 2800 3050
Wire Wire Line
	2800 3600 3450 3600
Connection ~ 3450 3600
Wire Wire Line
	2550 4550 2800 4550
$Comp
L power:GNDPWR #PWR021
U 1 1 5D2F1CB0
P 2550 4550
F 0 "#PWR021" H 2550 4350 50  0001 C CNN
F 1 "GNDPWR" H 2554 4396 50  0000 C CNN
F 2 "" H 2550 4500 50  0001 C CNN
F 3 "" H 2550 4500 50  0001 C CNN
	1    2550 4550
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q21
U 1 1 5D2F1CB6
P 2700 4350
F 0 "Q21" H 2950 4425 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 4350 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 4275 50  0001 L CIN
F 3 "" H 2700 4350 50  0001 L CNN
	1    2700 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 5100 2800 5100
$Comp
L power:GNDPWR #PWR022
U 1 1 5D2F1CBD
P 2550 5100
F 0 "#PWR022" H 2550 4900 50  0001 C CNN
F 1 "GNDPWR" H 2554 4946 50  0000 C CNN
F 2 "" H 2550 5050 50  0001 C CNN
F 3 "" H 2550 5050 50  0001 C CNN
	1    2550 5100
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q22
U 1 1 5D2F1CC3
P 2700 4900
F 0 "Q22" H 2950 4975 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 4900 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 4825 50  0001 L CIN
F 3 "" H 2700 4900 50  0001 L CNN
	1    2700 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4150 3450 4700
Wire Wire Line
	3450 4150 2800 4150
Wire Wire Line
	2800 4700 3450 4700
Connection ~ 3450 4700
Wire Wire Line
	3450 3600 3450 4150
Connection ~ 3450 4150
$Comp
L Device:R R20
U 1 1 5D2F1CCF
P 2350 3800
F 0 "R20" V 2250 3800 50  0000 C CNN
F 1 "65" V 2350 3800 50  0000 C CNN
F 2 "" V 2280 3800 50  0001 C CNN
F 3 "~" H 2350 3800 50  0001 C CNN
	1    2350 3800
	0    1    1    0   
$EndComp
$Comp
L Device:R R21
U 1 1 5D2F1CD5
P 2350 4350
F 0 "R21" V 2250 4350 50  0000 C CNN
F 1 "65" V 2350 4350 50  0000 C CNN
F 2 "" V 2280 4350 50  0001 C CNN
F 3 "~" H 2350 4350 50  0001 C CNN
	1    2350 4350
	0    1    1    0   
$EndComp
$Comp
L Device:R R22
U 1 1 5D2F1CDB
P 2350 4900
F 0 "R22" V 2250 4900 50  0000 C CNN
F 1 "65" V 2350 4900 50  0000 C CNN
F 2 "" V 2280 4900 50  0001 C CNN
F 3 "~" H 2350 4900 50  0001 C CNN
	1    2350 4900
	0    1    1    0   
$EndComp
Connection ~ 2200 4350
Wire Wire Line
	2200 4350 2200 4900
Wire Wire Line
	2200 3250 2200 3800
Connection ~ 2200 3800
Wire Wire Line
	2200 3800 2200 4350
Wire Wire Line
	3450 2500 3450 3050
Connection ~ 3450 3050
Wire Wire Line
	2200 2700 2200 3250
Connection ~ 2200 2700
Connection ~ 2200 3250
$Comp
L Device:R R23
U 1 1 5D2F1CEB
P 2350 5450
F 0 "R23" V 2250 5450 50  0000 C CNN
F 1 "65" V 2350 5450 50  0000 C CNN
F 2 "" V 2280 5450 50  0001 C CNN
F 3 "~" H 2350 5450 50  0001 C CNN
	1    2350 5450
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 5650 2800 5650
$Comp
L power:GNDPWR #PWR023
U 1 1 5D2F1CF2
P 2550 5650
F 0 "#PWR023" H 2550 5450 50  0001 C CNN
F 1 "GNDPWR" H 2554 5496 50  0000 C CNN
F 2 "" H 2550 5600 50  0001 C CNN
F 3 "" H 2550 5600 50  0001 C CNN
	1    2550 5650
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q23
U 1 1 5D2F1CF8
P 2700 5450
F 0 "Q23" H 2950 5525 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 5450 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 5375 50  0001 L CIN
F 3 "" H 2700 5450 50  0001 L CNN
	1    2700 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 6200 2800 6200
$Comp
L power:GNDPWR #PWR024
U 1 1 5D2F1CFF
P 2550 6200
F 0 "#PWR024" H 2550 6000 50  0001 C CNN
F 1 "GNDPWR" H 2554 6046 50  0000 C CNN
F 2 "" H 2550 6150 50  0001 C CNN
F 3 "" H 2550 6150 50  0001 C CNN
	1    2550 6200
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q24
U 1 1 5D2F1D05
P 2700 6000
F 0 "Q24" H 2950 6075 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 6000 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 5925 50  0001 L CIN
F 3 "" H 2700 6000 50  0001 L CNN
	1    2700 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 5250 3450 5800
Wire Wire Line
	3450 5250 2800 5250
Wire Wire Line
	2800 5800 3450 5800
Connection ~ 3450 5800
Wire Wire Line
	2550 6750 2800 6750
$Comp
L power:GNDPWR #PWR025
U 1 1 5D2F1D10
P 2550 6750
F 0 "#PWR025" H 2550 6550 50  0001 C CNN
F 1 "GNDPWR" H 2554 6596 50  0000 C CNN
F 2 "" H 2550 6700 50  0001 C CNN
F 3 "" H 2550 6700 50  0001 C CNN
	1    2550 6750
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q25
U 1 1 5D2F1D16
P 2700 6550
F 0 "Q25" H 2950 6625 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 6550 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 6475 50  0001 L CIN
F 3 "" H 2700 6550 50  0001 L CNN
	1    2700 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 7300 2800 7300
$Comp
L power:GNDPWR #PWR026
U 1 1 5D2F1D1D
P 2550 7300
F 0 "#PWR026" H 2550 7100 50  0001 C CNN
F 1 "GNDPWR" H 2554 7146 50  0000 C CNN
F 2 "" H 2550 7250 50  0001 C CNN
F 3 "" H 2550 7250 50  0001 C CNN
	1    2550 7300
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q26
U 1 1 5D2F1D23
P 2700 7100
F 0 "Q26" H 2950 7175 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 7100 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 7025 50  0001 L CIN
F 3 "" H 2700 7100 50  0001 L CNN
	1    2700 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 6350 3450 6900
Wire Wire Line
	3450 6350 2800 6350
Wire Wire Line
	2800 6900 3450 6900
Connection ~ 3450 6900
Wire Wire Line
	3450 5800 3450 6350
Connection ~ 3450 6350
$Comp
L Device:R R24
U 1 1 5D2F1D2F
P 2350 6000
F 0 "R24" V 2250 6000 50  0000 C CNN
F 1 "65" V 2350 6000 50  0000 C CNN
F 2 "" V 2280 6000 50  0001 C CNN
F 3 "~" H 2350 6000 50  0001 C CNN
	1    2350 6000
	0    1    1    0   
$EndComp
$Comp
L Device:R R25
U 1 1 5D2F1D35
P 2350 6550
F 0 "R25" V 2250 6550 50  0000 C CNN
F 1 "65" V 2350 6550 50  0000 C CNN
F 2 "" V 2280 6550 50  0001 C CNN
F 3 "~" H 2350 6550 50  0001 C CNN
	1    2350 6550
	0    1    1    0   
$EndComp
$Comp
L Device:R R26
U 1 1 5D2F1D3B
P 2350 7100
F 0 "R26" V 2250 7100 50  0000 C CNN
F 1 "65" V 2350 7100 50  0000 C CNN
F 2 "" V 2280 7100 50  0001 C CNN
F 3 "~" H 2350 7100 50  0001 C CNN
	1    2350 7100
	0    1    1    0   
$EndComp
Connection ~ 2200 6550
Wire Wire Line
	2200 6550 2200 7100
Wire Wire Line
	2200 5450 2200 6000
Connection ~ 2200 6000
Wire Wire Line
	2200 6000 2200 6550
$Comp
L Device:R R27
U 1 1 5D2F1D46
P 2350 7650
F 0 "R27" V 2250 7650 50  0000 C CNN
F 1 "65" V 2350 7650 50  0000 C CNN
F 2 "" V 2280 7650 50  0001 C CNN
F 3 "~" H 2350 7650 50  0001 C CNN
	1    2350 7650
	0    1    1    0   
$EndComp
Wire Wire Line
	2550 7850 2800 7850
$Comp
L power:GNDPWR #PWR027
U 1 1 5D2F1D4D
P 2550 7850
F 0 "#PWR027" H 2550 7650 50  0001 C CNN
F 1 "GNDPWR" H 2554 7696 50  0000 C CNN
F 2 "" H 2550 7800 50  0001 C CNN
F 3 "" H 2550 7800 50  0001 C CNN
	1    2550 7850
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q27
U 1 1 5D2F1D53
P 2700 7650
F 0 "Q27" H 2950 7725 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 7650 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 7575 50  0001 L CIN
F 3 "" H 2700 7650 50  0001 L CNN
	1    2700 7650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 8400 2700 8400
$Comp
L power:GNDPWR #PWR028
U 1 1 5D2F1D5A
P 2550 8400
F 0 "#PWR028" H 2550 8200 50  0001 C CNN
F 1 "GNDPWR" H 2554 8246 50  0000 C CNN
F 2 "" H 2550 8350 50  0001 C CNN
F 3 "" H 2550 8350 50  0001 C CNN
	1    2550 8400
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:IRF540N Q28
U 1 1 5D2F1D60
P 2700 8200
F 0 "Q28" H 2950 8275 50  0000 L CNN
F 1 "irfR7446pbf" H 2950 8200 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 2950 8125 50  0001 L CIN
F 3 "" H 2700 8200 50  0001 L CNN
	1    2700 8200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 7450 3450 8000
Wire Wire Line
	3450 7450 2800 7450
Wire Wire Line
	2800 8000 3450 8000
$Comp
L Device:R R28
U 1 1 5D2F1D69
P 2350 8200
F 0 "R28" V 2250 8200 50  0000 C CNN
F 1 "65" V 2350 8200 50  0000 C CNN
F 2 "" V 2280 8200 50  0001 C CNN
F 3 "~" H 2350 8200 50  0001 C CNN
	1    2350 8200
	0    1    1    0   
$EndComp
Wire Wire Line
	2200 7650 2200 8200
Wire Wire Line
	3450 6900 3450 7450
Connection ~ 3450 7450
Wire Wire Line
	2200 7100 2200 7650
Connection ~ 2200 7100
Connection ~ 2200 7650
Wire Wire Line
	3450 4700 3450 5250
Connection ~ 3450 5250
Wire Wire Line
	2200 4900 2200 5450
Connection ~ 2200 4900
Connection ~ 2200 5450
Wire Wire Line
	2000 850  2800 850 
Connection ~ 2800 850 
Text Label 2750 8800 0    59   ~ 0
MOSFET_source
Text Label 2850 8650 0    59   ~ 0
MOSFET_drain
Wire Wire Line
	3450 8650 2850 8650
Wire Wire Line
	3450 8000 3450 8650
Connection ~ 3450 8000
Wire Wire Line
	2700 8400 2700 8800
Wire Wire Line
	2700 8800 3450 8800
Connection ~ 2700 8400
Wire Wire Line
	2700 8400 2800 8400
Wire Wire Line
	750  8200 750  8800
Connection ~ 750  8200
Wire Wire Line
	2200 8200 2200 8800
Wire Wire Line
	750  8800 2200 8800
Connection ~ 2200 8200
$Comp
L Diode:1.5KExxA D1
U 1 1 5D86122C
P 3800 1150
F 0 "D1" H 3800 1250 50  0000 C CNN
F 1 "TVS SMDJ13A" H 3800 1050 50  0000 C CNN
F 2 "Diode_SMD:D_SMC" H 3800 1150 50  0001 C CNN
F 3 "" H 3800 1150 50  0000 C CNN
	1    3800 1150
	0    -1   1    0   
$EndComp
$Comp
L Diode:1.5KExxA D2
U 1 1 5D8BDB53
P 4100 1150
F 0 "D2" H 4100 1250 50  0000 C CNN
F 1 "TVS SMDJ13A" H 4100 1050 50  0000 C CNN
F 2 "Diode_SMD:D_SMC" H 4100 1150 50  0001 C CNN
F 3 "" H 4100 1150 50  0000 C CNN
	1    4100 1150
	0    -1   1    0   
$EndComp
$Comp
L Diode:1.5KExxA D3
U 1 1 5D91A069
P 4400 1150
F 0 "D3" H 4400 1250 50  0000 C CNN
F 1 "TVS SMDJ13A" H 4400 1050 50  0000 C CNN
F 2 "Diode_SMD:D_SMC" H 4400 1150 50  0001 C CNN
F 3 "" H 4400 1150 50  0000 C CNN
	1    4400 1150
	0    -1   1    0   
$EndComp
$Comp
L Diode:1.5KExxA D4
U 1 1 5D94823D
P 4700 1150
F 0 "D4" H 4700 1250 50  0000 C CNN
F 1 "TVS SMDJ13A" H 4700 1050 50  0000 C CNN
F 2 "Diode_SMD:D_SMC" H 4700 1150 50  0001 C CNN
F 3 "" H 4700 1150 50  0000 C CNN
	1    4700 1150
	0    -1   1    0   
$EndComp
$Comp
L Diode:1.5KExxA D5
U 1 1 5D9764F5
P 5000 1150
F 0 "D5" H 5000 1250 50  0000 C CNN
F 1 "TVS SMDJ13A" H 5000 1050 50  0000 C CNN
F 2 "Diode_SMD:D_SMC" H 5000 1150 50  0001 C CNN
F 3 "" H 5000 1150 50  0000 C CNN
	1    5000 1150
	0    -1   1    0   
$EndComp
Wire Wire Line
	4400 1500 4400 1450
Wire Wire Line
	4400 1450 4100 1450
Wire Wire Line
	4100 1450 4100 1300
Connection ~ 4400 1450
Wire Wire Line
	4400 1450 4400 1300
Wire Wire Line
	4100 1450 3800 1450
Wire Wire Line
	3800 1450 3800 1300
Connection ~ 4100 1450
Wire Wire Line
	4400 1450 4700 1450
Wire Wire Line
	4700 1450 4700 1300
Wire Wire Line
	4700 1450 5000 1450
Wire Wire Line
	5000 1450 5000 1300
Connection ~ 4700 1450
Wire Wire Line
	5000 1000 5000 850 
Wire Wire Line
	5000 850  4700 850 
Connection ~ 3450 850 
Wire Wire Line
	3800 1000 3800 850 
Connection ~ 3800 850 
Wire Wire Line
	3800 850  3450 850 
Wire Wire Line
	4100 1000 4100 850 
Connection ~ 4100 850 
Wire Wire Line
	4100 850  3800 850 
Wire Wire Line
	4400 1000 4400 850 
Connection ~ 4400 850 
Wire Wire Line
	4400 850  4100 850 
Wire Wire Line
	4700 1000 4700 850 
Connection ~ 4700 850 
Wire Wire Line
	4700 850  4400 850 
Text Notes 3550 3950 0    39   ~ 0
Paralleling N_tvs identical(!) LF SMDJ13A and the avalanche clamping:\n - breakdown: Vbr = 14.4V @ Ibr = 1mA\n - clamping:  Vc  = 21.5V @ Ic  = 139.5A\n - Lets plot a line where y = voltage, x = current\n    y = kx + n;  k = (Vc-Vbr)/(Ic-Ibr); n = y1 - kx1\n    Adding N_tvs TVS in parallel reduces slope grade\n     I=1500, N_tvs=4, Vbr = 14.4; Ibr=1e-3; Vc = 21.5; Ic = 139.5;\n     Ibr = N_tvs*Ibr; Ic = N_tvs*Ic; k = (Vc - Vbr) / (Ic - Ibr), n = Vc - k*Ic,\n     Vclamp = k*I +n\n - Results at 1500A for different numbers of TVS diodes:\n    Vclamp(N_tvs) = [90.8V, 52.6V, 39.8V, 33.5V, 29.7V, 27.1V]\n - As we can see, theoretically 3 diodes would be enough. (44V fet bd @ 20'C),\n   the problem is TVS current rating (max 400A fwd(!)) and contact resistance\n   we need 5 diodes to reach 2000A.\n  (note: assuming Vc rises linearly with current, somewhere we may hit package limits,\n   but the LF SMDJ13A DS specifies 400A as the peak forward(!) current, so it may be fine)\n\n- Avalanche energy on a loop of 100 cm 10mm^2 wire:\n  based on Infineon AN_201611_PL11_002 (deals with OPTIMOS, not StrongIRFET!)\n  L = 100; D = 0.2*sqrt(10/pi); I = 1500;\n  K=D/2/L;\n  Lwire = 1e-9 * 2*L*( log(2*L/D * (1 + sqrt(1 + K**2))) - sqrt(1 + K**2) + mu/4 + K))\n  E_AS = 1/2 * Lwire * I**2 = 1.36 J\n\n- Avalanche duration and shape?\n\nTODO\n- Add a flyback schottky from weld- to batt+? e.g. 100BGQ015\n\n- use THT TVS diodes? e.g. 5KP14A-E3/54\n\n- use >14.4V reverse standoff voltage TVS (SMDJ{14,15}A?)\n  to allow simultaneous charging of the 6S lead acid?\n  -> no need, SMDJ13A conduction at 14.4V is at most 1 mA. Insignificant.\n
$Comp
L Device:R R29
U 1 1 5DBC98F4
P 5650 850
F 0 "R29" V 5730 850 50  0000 C CNN
F 1 "1k" V 5650 850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 5580 850 50  0001 C CNN
F 3 "" H 5650 850 50  0001 C CNN
F 4 "MCF 0.25W 1K" H 3150 250 50  0001 C CNN "part"
F 5 "have, C57435" H 3150 250 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1001A50_C57435.html" H 3150 250 50  0001 C CNN "LCSC_web"
	1    5650 850 
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR042
U 1 1 5DBC98FA
P 6000 850
F 0 "#PWR042" H 6000 600 50  0001 C CNN
F 1 "GND" H 6000 700 50  0000 C CNN
F 2 "" H 6000 850 50  0001 C CNN
F 3 "" H 6000 850 50  0001 C CNN
	1    6000 850 
	1    0    0    -1  
$EndComp
Text Notes 5250 1150 0    39   ~ 0
weld cable discharge\n (for contact detect)\n12V/1k = 12 mA loss
Wire Wire Line
	6000 850  5800 850 
Wire Wire Line
	5500 850  5000 850 
Connection ~ 5000 850 
Wire Notes Line
	11750 5950 6750 5950
Text Notes 6800 3600 0    39   ~ 0
- Resistivity of copper at 20'C : p = 1.68e-8 ohm m, K = 0.00404 /kelvin\n- Resistance of 20 cm of 10mm^2 copper wire: \n  = p*l/A = 1.68e-8 * 0.2 / (10 / (1000*1000)) = 336 uohm, drop at 1000A = 336mV\n\n- MOSFET array expected RDSon\n  40xIRFR7446PBF @ 25-100'C : 25-34 uohm; 25-34mV drop at 1000A\n  10xIRFB7437PBF @ 25-100'C:  100-200 uohm, 100-200 mV drop at 1000A\n\n- full system resistance: 100cm of 10mm^2 wire: 1.7 mohm; 30 uohm of FET;\n  50 mohm for lead-acid and clamps => 250A current :(
Wire Notes Line
	11750 700  11750 5950
Wire Notes Line
	6750 700  6750 5950
Text Label 12400 5000 0    39   ~ 0
U_aux
Wire Wire Line
	12400 5000 12800 5000
Wire Wire Line
	12100 5700 12800 5700
$Comp
L Connector:Conn_01x04_Male J8
U 1 1 5E444F01
P 14150 3900
F 0 "J8" H 14258 4181 50  0000 C CNN
F 1 "SPI_conn" H 14258 4090 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Horizontal" H 14150 3900 50  0001 C CNN
F 3 "~" H 14150 3900 50  0001 C CNN
	1    14150 3900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR084
U 1 1 5E4D2F75
P 14400 4100
F 0 "#PWR084" H 14400 3850 50  0001 C CNN
F 1 "GND" H 14405 3927 50  0000 C CNN
F 2 "" H 14400 4100 50  0001 C CNN
F 3 "" H 14400 4100 50  0001 C CNN
	1    14400 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	14400 4100 14350 4100
Text Label 14550 3800 0    39   ~ 0
SPI_SCK
Text Label 14550 4000 0    39   ~ 0
SPI_MOSI
Text Label 14550 3900 0    39   ~ 0
SPI_MISO
Wire Wire Line
	14350 3900 14800 3900
Wire Wire Line
	14350 3800 14800 3800
Wire Wire Line
	14350 4000 14800 4000
Text Label 12450 5100 0    39   ~ 0
SPI_SCK
Text Label 12450 5300 0    39   ~ 0
SPI_MOSI
Text Label 12450 5200 0    39   ~ 0
SPI_MISO
Wire Wire Line
	12450 5100 12800 5100
Wire Wire Line
	12450 5200 12800 5200
Wire Wire Line
	12450 5300 12800 5300
Text Label 12450 5400 0    39   ~ 0
SCL_TX
Text Label 12450 5500 0    39   ~ 0
SDA_RX
Wire Wire Line
	12450 5400 12800 5400
Wire Wire Line
	12450 5500 12800 5500
Text Label 14550 3100 0    39   ~ 0
SCL_TX
Text Label 14550 3300 0    39   ~ 0
SDA_RX
Wire Wire Line
	14400 3100 14750 3100
Wire Wire Line
	14400 3300 14750 3300
$Comp
L Connector:Conn_01x03_Male J11
U 1 1 5E94737E
P 14200 3200
F 0 "J11" H 14300 3500 50  0000 C CNN
F 1 "UART/i2C_conn" H 14350 3400 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 14200 3200 50  0001 C CNN
F 3 "~" H 14200 3200 50  0001 C CNN
	1    14200 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR085
U 1 1 5E9B7531
P 14850 3200
F 0 "#PWR085" H 14850 2950 50  0001 C CNN
F 1 "GND" H 14855 3027 50  0000 C CNN
F 2 "" H 14850 3200 50  0001 C CNN
F 3 "" H 14850 3200 50  0001 C CNN
	1    14850 3200
	1    0    0    -1  
$EndComp
Wire Notes Line
	15950 2700 15950 4350
Wire Notes Line
	14050 2700 14050 4350
$Comp
L Device:Jumper JP6
U 1 1 5EC4ABCA
P 10650 6850
F 0 "JP6" H 10650 7000 50  0000 C CNN
F 1 "Jumper" H 10650 6750 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 10650 6850 50  0001 C CNN
F 3 "~" H 10650 6850 50  0001 C CNN
	1    10650 6850
	1    0    0    -1  
$EndComp
Wire Notes Line
	8650 6100 11450 6100
Wire Notes Line
	8650 7400 11450 7400
$Comp
L Jumper:SolderJumper_2_Bridged JP7
U 1 1 5ECE4D41
P 10850 7150
F 0 "JP7" H 10850 7250 50  0000 C CNN
F 1 "SolderJumper_2_Bridged" H 10850 7050 50  0000 C CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_Pad1.0x1.5mm" H 10850 7150 50  0001 C CNN
F 3 "~" H 10850 7150 50  0001 C CNN
F 4 "n/a" H 7600 5200 50  0001 C CNN "part"
	1    10850 7150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10700 7150 10350 7150
Wire Wire Line
	10350 7150 10350 6850
Connection ~ 10350 6850
Wire Wire Line
	11000 7150 11000 6850
Connection ~ 11000 6850
Wire Wire Line
	11000 6850 11400 6850
Text Notes 10500 7350 0    39   ~ 0
cap disconnect for SWD
Wire Wire Line
	14400 3200 14850 3200
Wire Wire Line
	3900 6050 3800 6050
Wire Wire Line
	4200 6050 4650 6050
Wire Wire Line
	3800 6350 4650 6350
$Comp
L Device:Opamp_Quad_Generic U2
U 3 1 5DA3413B
P 8900 5300
F 0 "U2" H 8900 5500 50  0000 C CNN
F 1 "quad opamp" H 8950 5100 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8900 5300 50  0001 C CNN
F 3 "~" H 8900 5300 50  0001 C CNN
F 4 "LM224ADT" H 1900 2850 50  0001 C CNN "part"
F 5 "LM224ADT" H 1900 2850 50  0001 C CNN "arrow"
	3    8900 5300
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Quad_Generic U2
U 4 1 5DA5B9E7
P 9850 5000
F 0 "U2" H 9850 5200 50  0000 C CNN
F 1 "quad opamp" H 9900 4800 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 9850 5000 50  0001 C CNN
F 3 "~" H 9850 5000 50  0001 C CNN
F 4 "LM224ADT" H 1900 2850 50  0001 C CNN "part"
F 5 "LM224ADT" H 1900 2850 50  0001 C CNN "arrow"
	4    9850 5000
	1    0    0    -1  
$EndComp
Text Notes 8300 5150 1    39   ~ 0
cca 370mV fwd drop @ 1mA
Wire Wire Line
	8200 4550 8150 4550
$Comp
L Device:D_Schottky_x2_Serial_ACK D15
U 1 1 5D98B3CB
P 8500 4550
F 0 "D15" H 8500 4766 50  0000 C CNN
F 1 "SDMP0340LST" H 8500 4675 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-416" H 8500 4550 50  0001 C CNN
F 3 "~" H 8500 4550 50  0001 C CNN
	1    8500 4550
	0    1    -1   0   
$EndComp
$Comp
L Device:D_Schottky_x2_Serial_ACK D12
U 1 1 5D98B3C3
P 7950 4550
F 0 "D12" H 7950 4766 50  0000 C CNN
F 1 "SDMP0340LST" H 7950 4675 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-416" H 7950 4550 50  0001 C CNN
F 3 "~" H 7950 4550 50  0001 C CNN
	1    7950 4550
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R43
U 1 1 5D98B3B0
P 9400 5300
F 0 "R43" V 9480 5300 50  0000 C CNN
F 1 "1k" V 9400 5300 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 9330 5300 50  0001 C CNN
F 3 "" H 9400 5300 50  0001 C CNN
F 4 "MCF 0.25W 1K" H 1900 2850 50  0001 C CNN "part"
F 5 "have, C57435" H 1900 2850 50  0001 C CNN "LCSC"
F 6 "https://lcsc.com/product-detail/Metal-Film-Resistor-TH_Uniroyal-Elec-MFR0W4F1001A50_C57435.html" H 1900 2850 50  0001 C CNN "LCSC_web"
	1    9400 5300
	0    -1   -1   0   
$EndComp
Text Notes 9550 4750 0    39   ~ 0
upper precision clipper
Wire Wire Line
	9250 5300 9250 5700
Wire Wire Line
	10150 5000 10200 5000
Wire Wire Line
	9400 4900 9550 4900
$Comp
L power:+3V3 #PWR059
U 1 1 5D98B370
P 9400 4900
F 0 "#PWR059" H 9400 4750 50  0001 C CNN
F 1 "+3V3" H 9415 5073 50  0000 C CNN
F 2 "" H 9400 4900 50  0001 C CNN
F 3 "" H 9400 4900 50  0001 C CNN
	1    9400 4900
	1    0    0    -1  
$EndComp
Connection ~ 9550 5300
Wire Wire Line
	9550 5300 9550 5100
Connection ~ 9250 5300
Wire Wire Line
	9200 5300 9250 5300
Wire Wire Line
	9550 5300 10200 5300
Wire Wire Line
	10200 5300 10800 5300
Connection ~ 10200 5300
$Comp
L Diode:1N4148 D18
U 1 1 5D98B367
P 10200 5150
F 0 "D18" H 10200 5366 50  0000 C CNN
F 1 "1N4148" H 10200 5275 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 10200 4975 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 10200 5150 50  0001 C CNN
F 4 "Diotec 1N4148" H 1900 2850 50  0001 C CNN "part"
	1    10200 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	8300 5450 8450 5450
Connection ~ 8300 5450
Wire Wire Line
	8300 4550 8300 5450
Wire Wire Line
	8600 5150 8600 5200
Wire Wire Line
	8200 5150 8600 5150
Connection ~ 8200 5150
Wire Wire Line
	8200 4550 8200 5150
Wire Wire Line
	7950 4900 7950 4850
Wire Wire Line
	8250 4250 8500 4250
Wire Wire Line
	7950 4250 8250 4250
Connection ~ 8250 4250
Wire Wire Line
	8250 4200 8250 4250
$Comp
L power:GND #PWR049
U 1 1 5D98B34D
P 7950 4900
F 0 "#PWR049" H 7950 4650 50  0001 C CNN
F 1 "GND" H 7955 4727 50  0000 C CNN
F 2 "" H 7950 4900 50  0001 C CNN
F 3 "" H 7950 4900 50  0001 C CNN
	1    7950 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR055
U 1 1 5D98B347
P 8500 4850
F 0 "#PWR055" H 8500 4600 50  0001 C CNN
F 1 "GND" H 8505 4677 50  0000 C CNN
F 2 "" H 8500 4850 50  0001 C CNN
F 3 "" H 8500 4850 50  0001 C CNN
	1    8500 4850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR051
U 1 1 5D98B341
P 8250 4200
F 0 "#PWR051" H 8250 4050 50  0001 C CNN
F 1 "VCC" H 8267 4373 50  0000 C CNN
F 2 "" H 8250 4200 50  0001 C CNN
F 3 "" H 8250 4200 50  0001 C CNN
	1    8250 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 5450 8300 5450
Wire Wire Line
	7850 5150 8200 5150
Wire Wire Line
	7800 2200 8150 2200
Wire Wire Line
	7800 2500 8250 2500
$Comp
L Diode:1N4148 D11
U 1 1 5F1EA747
P 4050 6050
F 0 "D11" H 4050 5950 50  0000 C CNN
F 1 "1N4148" H 4050 6175 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4050 5875 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4050 6050 50  0001 C CNN
F 4 "Diotec 1N4148" H -4250 3750 50  0001 C CNN "part"
	1    4050 6050
	-1   0    0    1   
$EndComp
Text Notes 3850 6300 0    39   ~ 0
.6V drop at 1mA
Wire Notes Line
	14600 9750 14600 8400
Wire Notes Line
	14600 8400 15900 8400
Wire Notes Line
	15900 8400 15900 9750
$Comp
L Connector:Conn_01x02_Male J7
U 1 1 5F56D471
P 13650 6400
F 0 "J7" H 13758 6581 50  0000 C CNN
F 1 "PortF header" H 13758 6490 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Horizontal" H 13650 6400 50  0001 C CNN
F 3 "~" H 13650 6400 50  0001 C CNN
	1    13650 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	13850 6400 14400 6400
Wire Wire Line
	13850 6500 14400 6500
Connection ~ 10100 6850
Wire Wire Line
	10100 6850 10350 6850
Wire Wire Line
	10400 3200 10400 3050
Wire Wire Line
	10400 3500 10400 3650
Wire Wire Line
	10400 3650 10700 3650
Wire Wire Line
	10400 3050 10700 3050
Wire Wire Line
	10700 3500 10700 3650
Connection ~ 10700 3650
Wire Wire Line
	10700 3650 10850 3650
Wire Wire Line
	10700 3200 10700 3050
Connection ~ 10700 3050
Wire Wire Line
	10700 3050 10850 3050
$EndSCHEMATC
